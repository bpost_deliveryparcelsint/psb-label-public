package be.bpost.isend.common.config;

import be.bpost.isend.psb.label.exception.LabelException;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;

import java.util.Base64;
import java.util.Map;
import java.util.Properties;

public class SecretPropertiesManager implements ApplicationListener<ApplicationPreparedEvent> {

    private static final Logger log = LoggerFactory.getLogger(SecretPropertiesManager.class);
    private static final String PROPERTY_ENABLED     = "secret.properties.enabled";
    private static final String PROPERTY_SECRET_NAME = "secret.properties.secret.name";
    private static final String PROPERTY_REGION      = "secret.properties.region";

    @Override
    public void onApplicationEvent(ApplicationPreparedEvent event) {

        ConfigurableEnvironment environment = event.getApplicationContext().getEnvironment();
        String enabled = environment.getProperty(PROPERTY_ENABLED);
        if (Boolean.valueOf(enabled)) {
            String secretName = environment.getProperty(PROPERTY_SECRET_NAME);
            String region = environment.getProperty(PROPERTY_REGION);
            log.info("Reading secret properties from AWS secret " + secretName + " (region " + region + ")");
            String secret = getSecretFromAws(secretName, region);
            Properties secretProperties = secretToProperties(secret);
            environment.getPropertySources().addFirst(new PropertiesPropertySource("secret.properties.manager", secretProperties));
        }
        else {
            log.info("Reading of properties from secret is disabled");
        }
    }

    @VisibleForTesting
    protected Properties secretToProperties(String secret) {
        ObjectMapper jsonMapper = new ObjectMapper();
        Properties properties = new Properties();

        try {
            Map<String, String> secretMap = jsonMapper.readValue(secret, new TypeReference<Map<String, String>>() {});
            for (Map.Entry<String, String> secretEntry : secretMap.entrySet()) {
                properties.put(secretEntry.getKey(), secretEntry.getValue());
            }
            return properties;
        } catch (JsonProcessingException e) {
            throw new LabelException(e);
        }
    }

    private static String getSecretFromAws(String secretName, String region) {

        AWSSecretsManager client  = AWSSecretsManagerClientBuilder.standard().withRegion(region).build();
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest().withSecretId(secretName);
        GetSecretValueResult getSecretValueResult = client.getSecretValue(getSecretValueRequest);
        if (getSecretValueResult.getSecretString() != null) {
            return getSecretValueResult.getSecretString();
        }
        else {
            return new String(Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
        }
    }
}
