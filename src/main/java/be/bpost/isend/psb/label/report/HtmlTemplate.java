package be.bpost.isend.psb.label.report;

public enum HtmlTemplate {

    MAIN_TEMPLATE("/mainLabel.htm"),
    DOMESTIC_TEMPLATE("/domestic_sub_label.html"),
    INTERNATIONAL_TEMPLATE("/international_sub_label.html"),
    INTERNATIONAL_PUGO_TEMPLATE("/international_pugo_sub_label.html"),
    PROOF_OF_PAYMENT_TEMPLATE("/proof_of_payment.html"),
    CN23_TEMPLATE("/cn23_sub_document.html");

    HtmlTemplate(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    private String resourcePath;

    public String getResourcePath() {
        return resourcePath;
    }
}
