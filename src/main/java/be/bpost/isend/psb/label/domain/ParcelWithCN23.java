package be.bpost.isend.psb.label.domain;

import javax.validation.Valid;

public class ParcelWithCN23 {

    @Valid
    private Parcel parcel;

    @Valid
    private CN23 cn23;

    public Parcel getParcel() { return parcel;}

    public void setParcel(Parcel parcel) { this.parcel = parcel;}

    public CN23 getCn23() { return cn23;}

    public void setCn23(CN23 cn23) { this.cn23 = cn23;}

}
