package be.bpost.isend.psb.label.report;

import be.bpost.isend.psb.label.exception.LabelException;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

@Component
public class TemplateCache {

    private Map<HtmlTemplate, String> templates;

    @PostConstruct
    @VisibleForTesting
    protected void loadHtmlTemplates() {
        Map<HtmlTemplate,String> map = Maps.newHashMap();
        for (HtmlTemplate template : HtmlTemplate.values()) {
            map.put(template, readHtmlTemplate(template));
        }
        templates = map;
    }

    public String getHtmlTemplate(HtmlTemplate template) {
        return templates.get(template);
    }

    private String readHtmlTemplate(HtmlTemplate htmlTemplate) {
        try {
            URL resource = TemplateCache.class.getResource(htmlTemplate.getResourcePath());
            String mainLabelPath = Paths.get(resource.toURI()).toFile().toString();
            return Files.readString(Paths.get(mainLabelPath), StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new LabelException("Error when loading resource at path " + htmlTemplate.getResourcePath() + ": " + e, e);
        }
    }
}
