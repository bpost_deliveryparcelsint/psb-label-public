package be.bpost.isend.psb.label;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JoinUtils {

    private JoinUtils() {
    }

    public static String joinWithSpaces(String... tokens) {
        return filterJoin(" ", tokens);
    }

    public static String filterJoin(String delimiter, String... tokens) {
        if (tokens == null) {
            return null;
        }
        List<String> filteredTokens = Arrays.stream(tokens)
                .filter(StringUtils::isNotBlank)
                .collect(Collectors.toList());
        if (filteredTokens.isEmpty()) {
            return null;
        }
        return String.join(delimiter, filteredTokens);
    }
}
