package be.bpost.isend.psb.label.domain;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class CN23DocumentInput {

    @NotNull
    private PageFormat pageFormat;

    @NotEmpty
    @Valid
    private List<CN23> cn23;

    @Min(value = 1, message = "numOfCopies should not be less than 1")
    @Max(value = 4, message = "numOfCopies should not be greater than 4")
    private Integer numOfCopies;

    public Integer getNumOfCopies() {
        return null == numOfCopies ? 2 : numOfCopies; }

    public void setNumOfCopies(Integer numOfCopies) { this.numOfCopies = numOfCopies; }

    public PageFormat getPageFormat() {
        return pageFormat;
    }

    public void setPageFormat(PageFormat pageFormat) {
        this.pageFormat = pageFormat;
    }

    public List<CN23> getCn23() { return cn23; }

    public void setCn23(List<CN23> cn23) { this.cn23 = cn23; }
}
