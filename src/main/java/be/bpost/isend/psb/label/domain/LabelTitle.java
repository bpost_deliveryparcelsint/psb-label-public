package be.bpost.isend.psb.label.domain;

public enum LabelTitle {
    SENDER("Afzender | Expéditeur | Absender","Sender"),
    RECEIVER("Klant/Client","Receiver"),
    VIA_PICK_UP_POINT("VIA AFHAALPUNT/POINT D’ENLEVEMENT<br>","VIA PICK-UP POINT<br>");

    private String domestic;
    private String international;

    LabelTitle(String domestic, String international){
        this.domestic = domestic;
        this.international = international;
    }

    public String getDomestic() { return domestic; }

    public String getInternational() { return international; }
}
