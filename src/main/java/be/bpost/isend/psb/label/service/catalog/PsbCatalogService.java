package be.bpost.isend.psb.label.service.catalog;

import be.bpost.isend.psb.label.domain.Language;
import be.bpost.isend.psb.order.openapi.psb.catalog.PSBProductCatalogItems;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.Collections;

@Service
public class PsbCatalogService {

    private final RestTemplate restTemplate;

    @Value("${psb.catalog.items.url}")
    private  String itemsUrl;

    @Value("${psb.api.key}")
    private String xApiKey;

    public PsbCatalogService(RestTemplateBuilder restTemplateBuilder,
                             @Value("${timeout.default.connect.millis}") int connectTimeoutInMillis,
                             @Value("${timeout.default.read.millis}") int readTimeoutInMillis){
        this.restTemplate = restTemplateBuilder.setConnectTimeout(Duration.ofMillis(connectTimeoutInMillis))
                .setReadTimeout(Duration.ofMillis(readTimeoutInMillis)).build();
    }

    public PSBProductCatalogItems getItems(Language language){
        HttpEntity request = new HttpEntity(getDefaultHeaders());
        ResponseEntity<PSBProductCatalogItems> response = this.restTemplate.exchange(itemsUrl, HttpMethod.GET, request, PSBProductCatalogItems.class, language);
        return response.getBody();
    }

    private HttpHeaders getDefaultHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        if (!StringUtils.isBlank(xApiKey)) {
            headers.set("x-api-key", xApiKey);
        }
        return headers;
    }
}
