package be.bpost.isend.psb.label.domain.validation;

import be.bpost.isend.psb.label.domain.Language;
import be.bpost.isend.psb.label.service.catalog.CatalogDataCache;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NonDeliveryInstructionValidator implements ConstraintValidator<NonDeliveryInstruction, String> {

    private CatalogDataCache catalogDataCache;

    @Autowired
    public NonDeliveryInstructionValidator(CatalogDataCache catalogDataCache){
        this.catalogDataCache = catalogDataCache;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return null!=catalogDataCache.getNonDeliveryInstructions(value, Language.EN);
    }


}
