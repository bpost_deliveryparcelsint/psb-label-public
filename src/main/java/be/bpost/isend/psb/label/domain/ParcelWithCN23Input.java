package be.bpost.isend.psb.label.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ParcelWithCN23Input {

    @NotNull
    private PageFormat pageFormat;

    @NotEmpty
    @Valid
    private List<ParcelWithCN23> parcelWithCN23;

    public List<ParcelWithCN23> getParcelWithCN23() { return parcelWithCN23;}

    public void setParcelWithCN23(List<ParcelWithCN23> parcelWithCN23) { this.parcelWithCN23 = parcelWithCN23;}

    public PageFormat getPageFormat() {
        return pageFormat;
    }

    public void setPageFormat(PageFormat pageFormat) {
        this.pageFormat = pageFormat;
    }
}
