package be.bpost.isend.psb.label.domain.validation;

import be.bpost.isend.psb.label.domain.Language;
import be.bpost.isend.psb.label.service.catalog.CatalogDataCache;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ShipmentTypeValidator implements ConstraintValidator<ShipmentType, String> {

    private CatalogDataCache catalogDataCache;

    @Autowired
    public ShipmentTypeValidator(CatalogDataCache catalogDataCache){
        this.catalogDataCache = catalogDataCache;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return null!=catalogDataCache.getShipmentTypes(value, Language.EN);
    }

}
