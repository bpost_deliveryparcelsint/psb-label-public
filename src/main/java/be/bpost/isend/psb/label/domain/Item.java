package be.bpost.isend.psb.label.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class Item {

    @NotBlank
    private String itemDescription;

    @NotNull
    private Integer quantity;

    @NotNull
    private BigDecimal weight;

    @NotNull
    private BigDecimal value;

    private String hsTariff;

    @NotBlank
    private String country;

    public String getItemDescription() { return itemDescription; }

    public void setItemDescription(String itemDescription) { this.itemDescription = itemDescription; }

    public Integer getQuantity() { return quantity; }

    public void setQuantity(Integer quantity) { this.quantity = quantity; }

    public BigDecimal getWeight() { return weight; }

    public void setWeight(BigDecimal weight) { this.weight = weight; }

    public BigDecimal getValue() { return value; }

    public void setValue(BigDecimal value) { this.value = value; }

    public String getHsTariff() { return hsTariff; }

    public void setHsTariff(String hsTariff) { this.hsTariff = hsTariff; }

    public String getCountry() { return country; }

    public void setCountry(String country) { this.country = country; }
}
