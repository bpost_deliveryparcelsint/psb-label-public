package be.bpost.isend.psb.label.domain;

import com.itextpdf.kernel.geom.PageSize;

public enum PageFormat {
    A4(PageSize.A4),
    A6(PageSize.A6);

    private PageSize pageSize;

    @SuppressWarnings("unused")
    PageFormat(PageSize pageSize){
        this.pageSize = pageSize;
    }

    public PageSize getPageSize() {
        return pageSize;
    }
}
