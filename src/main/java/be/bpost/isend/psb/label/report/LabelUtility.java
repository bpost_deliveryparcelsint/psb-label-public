
package be.bpost.isend.psb.label.report;

import be.bpost.isend.psb.label.domain.*;
import be.bpost.isend.psb.label.exception.LabelValidationException;
import be.bpost.isend.psb.label.service.catalog.CatalogDataCache;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.output.OutputException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static be.bpost.isend.psb.label.report.LabelConstants.*;
import static be.bpost.isend.psb.label.report.ReportConstants.fixedText;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;
import static org.apache.commons.lang3.StringUtils.defaultString;

@Component
public class LabelUtility {

    private static final Logger LOGGER = LoggerFactory.getLogger(LabelUtility.class);

    public static final String TR_CLOSE = "</tr>";
    public static final String TR_OPEN = "<tr>\n";
    private TemplateCache templateCache;
    private CatalogDataCache catalogDataCache;

    @Autowired
    public LabelUtility(TemplateCache templateCache, CatalogDataCache catalogDataCache) {
        this.templateCache = templateCache;
        this.catalogDataCache = catalogDataCache;
    }

    public List<String> setParcelLabelValues(ParcelLabelInput parcelLabelInput) throws BarcodeException, OutputException, IOException {

        String patchHtml = templateCache.getHtmlTemplate(HtmlTemplate.MAIN_TEMPLATE);
        PageFormat pageFormat = parcelLabelInput.getPageFormat();
        List<String> patchedHtmlList = new ArrayList<>();
        List<Parcel> allParcelList = parcelLabelInput.getParcels();

        List<Parcel> groupedParcel = allParcelList.stream()
                .collect(Collectors.groupingBy(Parcel::getLabelLayout))
                .entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .collect(Collectors.toList())
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());

        int labelPerPage = parcelLabelInput.getPageFormat().equals(PageFormat.A4) ? 4 : 1;

        List<List<Parcel>> lists = Lists.partition(groupedParcel, labelPerPage);

        for (List<Parcel> parcelList : lists) {
            String html = patchHtml;
            int count = 1;
            StringBuilder labelData = new StringBuilder();
            for (Parcel parcel : parcelList) {
                String label = "";
                Address sender = parcel.getSender();
                Address receiver = parcel.getReceiver();

                // Getting Label Layout (National/International/International-PUGO)
                ParcelLabelLayout parcelLabelLayout = parcel.getLabelLayout();
                if (parcelLabelLayout.equals(ParcelLabelLayout.PREPAID_DOMESTIC)) {
                    label = templateCache.getHtmlTemplate(HtmlTemplate.DOMESTIC_TEMPLATE);
                } else if (parcelLabelLayout.equals(ParcelLabelLayout.PREPAID_INTL_PUGO)) {
                    label = templateCache.getHtmlTemplate(HtmlTemplate.INTERNATIONAL_PUGO_TEMPLATE);
                } else {
                    label = templateCache.getHtmlTemplate(HtmlTemplate.INTERNATIONAL_TEMPLATE); // International Label
                }

                // div style
                if (pageFormat.equals(PageFormat.A4)) {
                    label = label.replace(DIV_STYLE, LabelDataHelper.getDivStyle(count));
                    html = html.replace(CSS_FILE, PARCEL_A4_CSS);
                } else {
                    label = label.replace(DIV_STYLE, LabelDataHelper.getDivStyle(6));
                    html = html.replace(CSS_FILE, PARCEL_A6_CSS);
                }
                // Parcel
                label = LabelDataHelper.setParcelInfo(label, parcel);

                // barcode
                label = label.replace(BAR_CODE_IMAGE, LabelDataHelper.getBarCodeImage(parcel.getBarcode()));

                // Sender Address
                label = LabelDataHelper.setSenderAddress(label, sender);

                // Receiver Address
                label = LabelDataHelper.setReceiverAddress(label, receiver, pageFormat, parcelLabelLayout);

                // User Data
                if (parcelLabelLayout.equals(ParcelLabelLayout.PREPAID_DOMESTIC)) {
                    if (Strings.isNullOrEmpty(receiver.getParcelLocker()) && Strings.isNullOrEmpty(receiver.getPickupPoint()) && Strings.isNullOrEmpty(parcel.getCodBarcode())) { // Normal Label

                        label = label.replace(KLANT, "");
                        label = label.replace("$via", BR);
                        label = label.replace(LEFT_BLOCK, "");
                        label = label.replace(HIDE_COD, NONE);
                        label = label.replace(PICK_UP, "");
                        label = label.replace(REC_COMPANY, Strings.isNullOrEmpty(receiver.getCompany()) ? "" : receiver.getCompany() + BR);

                    } else if (!Strings.isNullOrEmpty(receiver.getParcelLocker())) { // Parcel Locker : B247

                        label = label.replace(LEFT_BLOCK, parcelLabelInput.getPageFormat().equals(PageFormat.A4) ? LOCKER : LOCKER_A6);
                        label = label.replace(KLANT, "");
                        label = label.replace("$via", BR);
                        label = label.replace(HIDE_COD, NONE);
                        label = label.replace(PICK_UP, receiver.getParcelLocker() + BR);
                        label = label.replace(REC_COMPANY, "");

                    } else if (!Strings.isNullOrEmpty(receiver.getPickupPoint())) { // Pick-up point : PUGO

                        label = label.replace(LEFT_BLOCK, PUGO);
                        label = label.replace(KLANT, LabelTitle.RECEIVER.getDomestic());
                        label = label.replace("$via", LabelTitle.VIA_PICK_UP_POINT.getDomestic());
                        label = label.replace(PICK_UP, receiver.getPickupPoint() + BR);
                        label = label.replace(REC_COMPANY, "");

                        if (!Strings.isNullOrEmpty(parcel.getCodBarcode())) { // Pick-up point : PUGO + Cash On delivery : COD
                            label = LabelDataHelper.setCodDetails(pageFormat, parcel, label, receiver);
                        } else {
                            label = label.replace(HIDE_COD, NONE);
                        }

                    } else if (!Strings.isNullOrEmpty(parcel.getCodBarcode())) { // Cash on Delivery : COD
                        label = LabelDataHelper.setCodDetails(pageFormat, parcel, label, receiver);

                    }
                } else if (parcelLabelLayout.equals(ParcelLabelLayout.PREPAID_INTL)) {

                    label = setPrepaidInternationalData(catalogDataCache, parcel, label, sender, receiver);

                } else if (parcelLabelLayout.equals(ParcelLabelLayout.PREPAID_INTL_PUGO)) {
                    label = label.replace(PICK_UP, receiver.getPickupPoint() + BR);
                    label = label.replace(KLANT, LabelTitle.RECEIVER.getInternational());
                    label = label.replace("$via", LabelTitle.VIA_PICK_UP_POINT.getInternational());
                    label = label.replace("$recCompnay", "");
                    label = label.replace("$receiverCountry", catalogDataCache.getCountryName(receiver.getCountry(), Language.EN).toUpperCase());
                    label = label.replace("$senderCoun", catalogDataCache.getCountryName(sender.getCountry(), Language.EN).toUpperCase());
                    label = label.replace("$phoneNum", defaultIfEmpty(sender.getPhoneNum(), ""));
                    label = label.replace("$intCountCode", defaultIfEmpty(receiver.getCountry(), ""));

                } else {
                    throw new LabelValidationException("Provide correct layout");
                }

                labelData.append(label);
                count++;

            }
            html = html.replace(LABEL_DATA, labelData);

            patchedHtmlList.add(html);

        }
        return patchedHtmlList;
    }

    public List<String> setProofOfPaymentValues(ProofOfPaymentInput proofOfPaymentInput) {
        String html = templateCache.getHtmlTemplate(HtmlTemplate.PROOF_OF_PAYMENT_TEMPLATE);
        List<String> patchedHtml = new ArrayList<>();
        String language = proofOfPaymentInput.getLanguage().getLang();
        List<ProductDetails> productDetails = proofOfPaymentInput.getProductDetails();
        Address yourDetails = proofOfPaymentInput.getSender();

        // Order details - Fix Text
        html = html.replace("$orderConfirmation", fixedText.get(language + "_ORDER_CONFIRMATION"));
        html = html.replace("$ordNum", fixedText.get(language + "_ORDER_NUMBER"));
        html = html.replace("$status", fixedText.get(language + "_STATUS"));
        html = html.replace("$paidvia", fixedText.get(language + "_PAID_VIA"));
        html = html.replace("$date", fixedText.get(language + "_DATE"));
        // Order details - Dynamic Text
        html = html.replace("$oNum", proofOfPaymentInput.getOrderNumber());
        html = html.replace("$stat", proofOfPaymentInput.getPaymentMethod());
        html = html.replace("$dte", proofOfPaymentInput.getDate());
        html = html.replace("$cardDetails", Strings.isNullOrEmpty(proofOfPaymentInput.getCardNumber()) ? "" : proofOfPaymentInput.getCardNumber() + BR);

        // Bpost details - Fix Text
        html = html.replace("$bpostDetails", fixedText.get(language + "_BPOST_DETAILS"));
        html = html.replace("$street1", fixedText.get(language + "_STREET_1"));
        html = html.replace("$street2", fixedText.get(language + "_STREET_2"));
        html = html.replace("$postalCity", fixedText.get(language + "_POSTAL_CITY"));
        html = html.replace("$telNo", fixedText.get("TEL_NO"));
        html = html.replace("$tva", fixedText.get(language + "_TVA"));
        html = html.replace("$website", fixedText.get(language + "_WEBSITE"));

        // Your details - Fix Text
        html = html.replace("$yourDetails", fixedText.get(language + "_YOUR_DETAILS"));
        // Your details - Dynamic Text
        html = html.replace("$yourFirstName", defaultIfEmpty(yourDetails.getFirstName(), ""));
        html = html.replace("$yourLastName", defaultIfEmpty(yourDetails.getLastName(), ""));
        html = html.replace("$yourCom", Strings.isNullOrEmpty(yourDetails.getCompany()) ? "" : yourDetails.getCompany() + BR);
        html = html.replace("$yourStreet", yourDetails.getStreet());
        html = html.replace("$yorStrtNum", defaultIfEmpty(yourDetails.getStreetNumber(), ""));
        html = html.replace("$yourBox", LabelDataHelper.formatBox(yourDetails.getBox()));
        html = html.replace("$yourPostalCode", yourDetails.getPostalCode());
        html = html.replace("$yourCity", yourDetails.getCity());
        html = html.replace("$vtnum", defaultIfEmpty(proofOfPaymentInput.getVatNumber(), "BE"));

        // Footer - Fixed Text
        html = html.replace("$upFoot", fixedText.get(language + "_FOOTER_1"));
        html = html.replace("$downFoot", fixedText.get(language + "_FOOTER_2"));

        // Table - Fixed Text
        html = html.replace("$productDescription", fixedText.get(language + "_PRODUCT_DESCRIPTION"));
        html = html.replace("$quantity", fixedText.get(language + "_QUANTITY"));
        html = html.replace("$pricePerUnit", fixedText.get(language + "_PRICE_PER_UNIT"));
        html = html.replace("$totalAmount", fixedText.get(language + "_TOTAL_AMOUNT"));
        html = html.replace("$vatInc", fixedText.get(language + "_VAT_INCLU"));
        html = html.replace("$vatExc", fixedText.get(language + "_VAT_EXCLU"));
        html = html.replace("$vatsq", fixedText.get(language + "_VAT_SQ"));
        html = html.replace("$exemptFrom", fixedText.get(language + "_EXEMPT_FROM"));

        // vat exclusive
        html = html.replace("$exevat", LabelDataHelper.formatAmount(proofOfPaymentInput.getVatExclude()));
        // vat inclusive - promotion discount
        html = html.replace("$invat", LabelDataHelper.formatAmount(proofOfPaymentInput.getTotalAmount() != null ? proofOfPaymentInput.getTotalAmount() : proofOfPaymentInput.getVatInclusive()));
        // vat sq
        html = html.replace("$vt", LabelDataHelper.formatAmount(proofOfPaymentInput.getVatTotal()));

        var lastData = productDetails.size();
        var count = 1;
        StringBuilder tableData = new StringBuilder();
        for (ProductDetails details : productDetails) {
        
            if (lastData != count) {
                tableData.append(TR_OPEN +
                        "<td class=\"td-1 no-border\">" + details.getProductDescription() + TD_CLOSE +
                        "<td class=\"td-2 no-border\">" + details.getQuantity() + TD_CLOSE +
                        "<td class=\"td-3 no-border\">" + LabelDataHelper.formatAmount(details.getPricePerUnit()) + TD_CLOSE +
                        "<td class=\"td-4 no-border\">" + LabelDataHelper.formatAmount(details.getTotalAmount()) + TD_CLOSE +
                        TR_CLOSE);
            } else {
                tableData.append(TR_OPEN +
                        "<td class=\"td-1 td-norm-additional-margin no-border\">" + details.getProductDescription() + TD_CLOSE +
                        "<td class=\"td-2 td-norm-additional-margin no-border\">" + details.getQuantity() + TD_CLOSE +
                        "<td class=\"td-3 td-norm-additional-margin no-border\">" + LabelDataHelper.formatAmount(details.getPricePerUnit()) + TD_CLOSE +
                        "<td class=\"td-4 td-norm-additional-margin no-border\">" + LabelDataHelper.formatAmount(details.getTotalAmount()) + TD_CLOSE +
                        TR_CLOSE);
            }
            count++;
        }

        html = html.replace("$tableData", tableData);

        StringBuilder promocodeData = new StringBuilder();
        if (proofOfPaymentInput.getDiscountAmount() != null) {
            promocodeData.append(TR_OPEN +
                    "<td class=\"td-1 border\">" + fixedText.get(language + "_PROMO_DISCOUNT") + TD_CLOSE +
                    "<td class=\"td-2 border\">" + "" + TD_CLOSE +
                    "<td class=\"td-3 border\">" + "" + TD_CLOSE +
                    "<td class=\"td-4 border\">" + LabelDataHelper.formatAmount(proofOfPaymentInput.getDiscountAmount().negate()) + TD_CLOSE +
                    TR_CLOSE);
        }
        html = html.replace("$promocodeData", promocodeData);
        patchedHtml.add(html);

        return patchedHtml;
    }

    public List<String> setCN23DocumentValues(CN23DocumentInput cn23documentInput) throws BarcodeException, OutputException, IOException {

        String patchHtml = templateCache.getHtmlTemplate(HtmlTemplate.MAIN_TEMPLATE);
        String subLabel = templateCache.getHtmlTemplate(HtmlTemplate.CN23_TEMPLATE);
        PageFormat pageFormat = cn23documentInput.getPageFormat();
        List<String> patchedHtmlList = new ArrayList<>();
        List<CN23> oldList = cn23documentInput.getCn23();
        List<CN23> allCN23List = oldList.stream()
                .flatMap(i -> Collections.nCopies(cn23documentInput.getNumOfCopies(), i).stream())
                .collect(Collectors.toList());

        int labelPerPage = cn23documentInput.getPageFormat().equals(PageFormat.A4) ? 4 : 1;
        List<List<CN23>> lists = Lists.partition(allCN23List, labelPerPage);

        for (List<CN23> cn23List : lists) {
            String html = patchHtml;
            int count = 1;
            StringBuilder labelData = new StringBuilder();
            for (CN23 cn23 : cn23List) {
                String label = subLabel;
                // div style
                if (pageFormat.equals(PageFormat.A4)) {
                    label = label.replace(DIV_STYLE, LabelDataHelper.getDivStyle(count));
                    html = html.replace(CSS_FILE, CN23_A4_CSS);
                } else {
                    label = label.replace(DIV_STYLE, LabelDataHelper.getDivStyle(6));
                    html = html.replace(CSS_FILE, CN23_A6_CSS);
                }
                label = prepareCN23Label(catalogDataCache, cn23, label);
                labelData.append(label);
                count++;
            }
            html = html.replace(LABEL_DATA, labelData);

            patchedHtmlList.add(html);

        }
        return patchedHtmlList;
    }

    public List<String> setParcelWithCN23Values(ParcelWithCN23Input parcelWithCN23Input) throws OutputException, IOException, BarcodeException {

        // html templates
        String mainHtml = templateCache.getHtmlTemplate(HtmlTemplate.MAIN_TEMPLATE);
        String internationalHtml = templateCache.getHtmlTemplate(HtmlTemplate.INTERNATIONAL_TEMPLATE);
        String cn23Html = templateCache.getHtmlTemplate(HtmlTemplate.CN23_TEMPLATE);

        List<String> patchedHtmlList = new ArrayList<>();

        List<ParcelWithCN23> parcelWithCN23 = parcelWithCN23Input.getParcelWithCN23();
        PageFormat pageFormat = parcelWithCN23Input.getPageFormat();

        // Setting css style
        String css = "";
        if (pageFormat.equals(PageFormat.A4)) {
            css = CN23_A4_CSS + "\n" + PARCEL_A4_CSS;
        } else {
            css = PARCEL_A6_CSS + "\n" + CN23_A6_CSS;
        }
        mainHtml = mainHtml.replace(CSS_FILE, css);

        StringBuilder parcelCn23 = new StringBuilder();

        int labelPerPage = parcelWithCN23Input.getPageFormat().equals(PageFormat.A4) ? 2 : 1;

        List<List<ParcelWithCN23>> allParcelWithCN23 = Lists.partition(parcelWithCN23, labelPerPage);

        for (List<ParcelWithCN23> listParcelWithCN23 : allParcelWithCN23) {
            int count = 1;
            for (ParcelWithCN23 allData : listParcelWithCN23) {
                String cn23Label = cn23Html;
                String parcelLabel = internationalHtml;

                Parcel parcel = allData.getParcel();
                Address parcelSender = parcel.getSender();
                Address parcelReceiver = parcel.getReceiver();
                CN23 cn23 = allData.getCn23();

                count = count == 4 ? 1 : count;
                // div style for label
                if (pageFormat.equals(PageFormat.A4)) {
                    parcelLabel = parcelLabel.replace(DIV_STYLE, LabelDataHelper.getDivStyle(count));
                } else {
                    parcelLabel = parcelLabel.replace(DIV_STYLE, LabelDataHelper.getDivStyle(6));
                }
                // Parcel
                parcelLabel = LabelDataHelper.setParcelInfo(parcelLabel, parcel);
                // barcode
                parcelLabel = parcelLabel.replace(BAR_CODE_IMAGE, LabelDataHelper.getBarCodeImage(parcel.getBarcode()));
                // Sender Address
                parcelLabel = LabelDataHelper.setSenderAddress(parcelLabel, parcelSender);
                // Receiver Address
                parcelLabel = LabelDataHelper.setReceiverAddress(parcelLabel, parcelReceiver, PageFormat.A4, ParcelLabelLayout.CONTRACTUAL_INTL);
                //  User data - international label
                parcelLabel = setPrepaidInternationalData(catalogDataCache, parcel, parcelLabel, parcelSender, parcelReceiver);
                count++;

                // div style for cn23
                if (pageFormat.equals(PageFormat.A4)) {
                    cn23Label = cn23Label.replace(DIV_STYLE, LabelDataHelper.getDivStyle(count));
                } else {
                    cn23Label = cn23Label.replace(DIV_STYLE, LabelDataHelper.getDivStyle(6));
                }

                cn23Label = prepareCN23Label(catalogDataCache, cn23, cn23Label);
                count++;

                parcelCn23.append(parcelLabel);
                parcelCn23.append(cn23Label);
            }
        }
        mainHtml = mainHtml.replace(LABEL_DATA, parcelCn23);
        patchedHtmlList.add(mainHtml);
        return patchedHtmlList;
    }

    private static String prepareCN23Label(CatalogDataCache catalogDataCache, CN23 cn23, String label) throws OutputException, BarcodeException, IOException {
        Address sender = cn23.getSender();
        Address receiver = cn23.getReceiver();
        List<Item> itemList = cn23.getItems();
        label = label.replace("$category", computeProductCategory(cn23.getCategory()).getAbbreviation());
        // barcode
        label = label.replace(BAR_CODE_IMAGE, LabelDataHelper.getBarCodeImage(cn23.getBarcode()));
        label = label.replace("$barNumber", cn23.getBarcode());
        // date
        label = label.replace("$date", cn23.getDate());
        // Sender Address
        label = label.replace("$senderFullName", LabelDataHelper.setFullName(sender, 29));
        label = label.replace("$senCompnay", Strings.isNullOrEmpty(sender.getCompany()) ? "" : LabelDataHelper.truncateData(sender.getCompany(), 27) + BR);
        label = label.replace("$senderAddress", LabelDataHelper.setFullAddress(sender, 25));
        label = label.replace("$senderPostalCode", sender.getPostalCode());
        label = label.replace("$senderCity", sender.getCity());
        label = label.replace("$senderCoun", catalogDataCache.getCountryName(sender.getCountry(), Language.EN).toUpperCase());
        label = label.replace("$senderPhoneNumber", LabelDataHelper.formatWithDelimiter(sender.getPhoneNum(), sender.getEmail()));

        // Receiver Address
        label = label.replace("$receiverFullName", LabelDataHelper.setFullName(receiver, 40));
        label = label.replace("$recBuss", Strings.isNullOrEmpty(receiver.getCompany()) ? "" : LabelDataHelper.truncateData(receiver.getCompany(), 35) + BR);
        label = label.replace("$receiverAddress", LabelDataHelper.setFullAddress(receiver, 80) + hypen(receiver));
        label = label.replace("$additAddr", LabelDataHelper.formatWithDelimiter(LabelDataHelper.truncateData(receiver.getAddressOne(), 20), LabelDataHelper.truncateData(receiver.getAddressTwo(), 20)));
        label = label.replace("$receiverPostalCode", receiver.getPostalCode());
        label = label.replace("$receiverCity", receiver.getCity());
        label = label.replace("$receiverCoun", catalogDataCache.getCountryName(receiver.getCountry(), Language.EN).toUpperCase());
        label = label.replace("$recPho", LabelDataHelper.formatWithDelimiter(receiver.getPhoneNum(), receiver.getEmail()));

        // Items
        int itemNum = 1;
        for (Item item : itemList) {
            label = label.replace("$iteDe" + itemNum, defaultIfEmpty(item.getItemDescription(), ""));
            label = label.replace("$iteQu" + itemNum, null == item.getQuantity() ? "" : item.getQuantity().toString());
            label = label.replace("$iteWe" + itemNum, null == item.getWeight() ? "" : LabelDataHelper.formatAmount(item.getWeight()));
            label = label.replace("$iteVa" + itemNum, LabelDataHelper.formatAmount(item.getValue()));
            label = label.replace("$iteHs" + itemNum, defaultIfEmpty(item.getHsTariff(), ""));
            label = label.replace("$iteCo" + itemNum, catalogDataCache.getCountryName(item.getCountry(), Language.EN));
            itemNum++;
        }
        for (int i = itemNum; i <= 5; i++) {
            label = label.replace("$iteDe" + i, "");
            label = label.replace("$iteQu" + i, "");
            label = label.replace("$iteWe" + i, "");
            label = label.replace("$iteVa" + i, "");
            label = label.replace("$iteHs" + i, "");
            label = label.replace("$iteCo" + i, "");
        }
        // Shipment Type
        label = label.replace("$shipmentType", catalogDataCache.getShipmentTypes(cn23.getShipmentType(), Language.EN));
        if (cn23.getShipmentType().equals("OTHER")) {
            label = label.replace("$otrship", "\t\tReason : " + defaultIfEmpty(cn23.getOtherShipmentType(), ""));
        } else {
            label = label.replace("$otrship", "");
        }

        // Content Description
        label = label.replace("$conDesc", cn23.getContentDescription());
        // Total Gross Weight and Total Value
        label = label.replace("$totalGrsWei", LabelDataHelper.formatAmount(cn23.getTotalGrossWeight()));
        label = label.replace("$totlVal", LabelDataHelper.formatAmount(cn23.getTotalValue()));
        // Postal Charges
        label = label.replace("$posChar", LabelDataHelper.formatAmount(cn23.getPostalCharges()));
        // Non Delivery Instruction
        label = label.replace("$nonDeliveryInstruction", catalogDataCache.getNonDeliveryInstructions(cn23.getNonDeliveryInstruction(), Language.EN));
        return label;
    }

    private static String setPrepaidInternationalData(CatalogDataCache catalogDataCache, Parcel parcel, String label, Address sender, Address receiver) {

        if (parcel.getInternationalAddress() == null) {
            throw new LabelValidationException("InternationalAddress is missing");
        }
        InternationalAddress internationalAddress = parcel.getInternationalAddress();
        label = label.replace("$accountId", defaultIfEmpty(internationalAddress.getAccountId(), ""));
        label = label.replace("$nonDeliveryInstruction", catalogDataCache.getNonDeliveryInstructions(internationalAddress.getNonDeliveryInstruction(), Language.EN));
        label = label.replace("$sndCoun", catalogDataCache.getCountryName(sender.getCountry(), Language.EN).toUpperCase());
        label = label.replace("$sendCont", defaultIfEmpty(sender.getPhoneNum(), ""));
        label = label.replace("$conDesc", LabelDataHelper.formatContentDescription(internationalAddress.getContentDescription()));
        label = label.replace(REC_COMPANY, Strings.isNullOrEmpty(receiver.getCompany()) ? "" : receiver.getCompany() + BR);

        InternationalProductInfo productInfo = computeInternationalProductInformation(parcel);
        label = label.replace("$labelPrduct", productInfo.getProductLabel());
        label = label.replace("$category", productInfo.getCategory().getAbbreviation());
        label = label.replace("$epgLogo", productInfo.isShowEPGLogo() ? EPG_LOGO : "");
        label = label.replace("$additionalText", productInfo.getAdditionalText());

        if (catalogDataCache.isDestinationInEuropeanUnion(receiver.getCountry())) {
            label = label.replace("$showEuMsg", NONE);
        } else {
            label = label.replace("$showEuMsg", BLOCK);
        }
        label = label.replace("$recCon", catalogDataCache.getCountryName(receiver.getCountry(), Language.EN).toUpperCase());
        label = label.replace("$recPho", defaultIfEmpty(receiver.getPhoneNum(), ""));
        label = label.replace("$addOne", Strings.isNullOrEmpty(receiver.getAddressOne()) ? "" : receiver.getAddressOne() + BR);
        label = label.replace("$adTwo", Strings.isNullOrEmpty(receiver.getAddressTwo()) ? "" : receiver.getAddressTwo() + BR);
        label = label.replace("$isoCoun", receiver.getCountry());
        return label;
    }

    private static InternationalProductInfo computeInternationalProductInformation(Parcel parcel) {
        InternationalAddress internationalAddress = parcel.getInternationalAddress();
        String productLabel = defaultString(internationalAddress.getProductName(), "International Standard");
        Category category = computeProductCategory(parcel.getCategory());
        boolean showEPGLogo = Category.PARCEL == category;
        String additionalText = "";
        if (ParcelLabelLayout.CONTRACTUAL_INTL == parcel.getLabelLayout() && !internationalAddress.iseParcelGroupLabel()) {
            // Contractual "express" product (not EPG)
            showEPGLogo = false;
            additionalText = ADDITIONAL_TEXT_FOR_EXPRESS;
        }
        return new InternationalProductInfo(productLabel, category, showEPGLogo, additionalText);
    }

    private static Category computeProductCategory(String categoryCode) {
        if (StringUtils.isBlank(categoryCode)) {
            return Category.PARCEL; // Default category is PARCEL when not specified otherwise
        }
        try {
            return Category.valueOf(categoryCode);
        } catch (IllegalArgumentException e) {
            LOGGER.warn("Invalid category code: {}, defaulting to PARCEL...", categoryCode);
            return Category.PARCEL;
        }
    }

    private static String hypen(Address address) {
        return !Strings.isNullOrEmpty(address.getAddressOne()) || !Strings.isNullOrEmpty(address.getAddressTwo()) ? " - " : "";
    }
}
