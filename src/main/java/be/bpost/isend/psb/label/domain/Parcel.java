package be.bpost.isend.psb.label.domain;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class Parcel {

    @NotBlank
    private String barcode;

    private String validityDate;

    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @NotNull
    @Valid
    private Address sender;

    @NotNull
    @Valid
    private Address receiver;

    @NotNull
    @Min(0)
    @Max(30)
    private Integer maxWeight;

    @NotNull
    private ParcelType parcelType;

    private boolean extraWeight;
    private boolean captureSignature;

    // For COD Label
    private String codBarcode;
    private BigDecimal codAmount;
    private String codBankAccount;

    @NotNull
    private ParcelLabelLayout labelLayout;

    private InternationalAddress internationalAddress;

    public Parcel() {
        /**/
    }

    /** Getters and Setters*/


    public InternationalAddress getInternationalAddress() { return internationalAddress; }

    public void setInternationalAddress(InternationalAddress internationalAddress) { this.internationalAddress = internationalAddress; }
    public ParcelLabelLayout getLabelLayout() { return labelLayout; }

    public void setLabelLayout(ParcelLabelLayout labelLayout) { this.labelLayout = labelLayout; }

    public String getCodBarcode() { return codBarcode; }

    public void setCodBarcode(String codBarcode) {
        this.codBarcode = codBarcode;
    }

    public BigDecimal getCodAmount() {  return codAmount; }

    public void setCodAmount(BigDecimal codAmount) { this.codAmount = codAmount; }

    public String getCodBankAccount() {
        return codBankAccount;
    }

    public void setCodBankAccount(String codBankAccount) {
        this.codBankAccount = codBankAccount;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(String validityDate) {
        this.validityDate = validityDate;
    }

    public Address getSender() {
        return sender;
    }

    public void setSender(Address sender) {
        this.sender = sender;
    }

    public Address getReceiver() {
        return receiver;
    }

    public void setReceiver(Address receiver) {
        this.receiver = receiver;
    }

    public Integer getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(Integer maxWeight) { this.maxWeight = maxWeight; }

    public ParcelType getParcelType() {
        return parcelType;
    }

    public void setParcelType(ParcelType parcelType) {
        this.parcelType = parcelType;
    }

    public boolean isExtraWeight() {
        return extraWeight;
    }

    public void setExtraWeight(boolean extraWeight) {
        this.extraWeight = extraWeight;
    }

    public boolean isCaptureSignature() {
        return captureSignature;
    }

    public void setCaptureSignature(boolean captureSignature) {
        this.captureSignature = captureSignature;
    }


}
