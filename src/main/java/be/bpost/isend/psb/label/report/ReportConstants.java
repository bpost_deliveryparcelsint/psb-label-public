package be.bpost.isend.psb.label.report;

import java.util.HashMap;
import java.util.Map;

public class ReportConstants {

    protected static final Map<String,String> fixedText = new HashMap<>();

    static {
     fixedText.put("TEL_NO","Tel : 02/2012345");
    // Order details - EN
    fixedText.put("EN_ORDER_CONFIRMATION","Order confirmation");
    fixedText.put("EN_ORDER_NUMBER","Order number");
    fixedText.put("EN_STATUS","Status");
    fixedText.put("EN_DATE","Date");
    fixedText.put("EN_PAID_VIA","Paid via");
    // Order details - FR
    fixedText.put("FR_ORDER_CONFIRMATION","Confirmation de votre\n" +"commande");
    fixedText.put("FR_ORDER_NUMBER","N° commande");
    fixedText.put("FR_STATUS","Etat");
    fixedText.put("FR_DATE","Date");
    fixedText.put("FR_PAID_VIA","Payé via");
    // Order details - NL
    fixedText.put("NL_ORDER_CONFIRMATION","Orderbevestiging");
    fixedText.put("NL_ORDER_NUMBER","Ordernummer");
    fixedText.put("NL_STATUS","Status");
    fixedText.put("NL_DATE","Datum");
    fixedText.put("NL_PAID_VIA","Betaald via");

    // Bpost Details - EN
    fixedText.put("EN_BPOST_DETAILS","bpost details");
    fixedText.put("EN_STREET_1","bpost S.A. société de droit public");
    fixedText.put("EN_STREET_2","MULTI");
    fixedText.put("EN_POSTAL_CITY","1000 Bruxelles");
    fixedText.put("EN_TVA","TVA: BE 0 214 596 464,");
    fixedText.put("EN_WEBSITE","https://parcel.bpost.be/en");
    // Bpost Details - FR
    fixedText.put("FR_BPOST_DETAILS","Coordonnées bpost");
    fixedText.put("FR_STREET_1","bpost S.A. société de droit public");
    fixedText.put("FR_STREET_2","MULTI");
    fixedText.put("FR_POSTAL_CITY","1000 Bruxelles");
    fixedText.put("FR_TVA","TVA: BE 0 214 596 464,");
    fixedText.put("FR_WEBSITE","www.bpost.be/envoyermespaquets");
    // Bpost Details - NL
    fixedText.put("NL_BPOST_DETAILS","bpost gegevens");
    fixedText.put("NL_STREET_1","bpost N.V. van publiek recht");
    fixedText.put("NL_STREET_2","MULTI");
    fixedText.put("NL_POSTAL_CITY","1000 Brussel");
    fixedText.put("NL_TVA","btw: BE 0 214 596 464,");
    fixedText.put("NL_WEBSITE","www.bpost.be/pakjesversturen");

    // Your details -
    fixedText.put("EN_YOUR_DETAILS","Your details");
    fixedText.put("FR_YOUR_DETAILS","Vos coordonnées");
    fixedText.put("NL_YOUR_DETAILS","Contactgegevens");

    // Table - EN
    fixedText.put("EN_PRODUCT_DESCRIPTION","Product description");
    fixedText.put("EN_QUANTITY","Quantity");
    fixedText.put("EN_PRICE_PER_UNIT","Price per unit (EUR)");
    fixedText.put("EN_TOTAL_AMOUNT","Total amount (EUR)");
    fixedText.put("EN_VAT_INCLU","TOTAL VAT inclusive");
    fixedText.put("EN_VAT_EXCLU","TOTAL VAT excluded");
    fixedText.put("EN_VAT_SQ","TOTAL VAT");
    fixedText.put("EN_EXEMPT_FROM","Exempt from VAT (Article 44 of the VAT Code)");
    fixedText.put("EN_PROMO_DISCOUNT","Promotion code");
    // Table - FR
    fixedText.put("FR_PRODUCT_DESCRIPTION","Description produit");
    fixedText.put("FR_QUANTITY","Quantité");
    fixedText.put("FR_PRICE_PER_UNIT","Prix unitaire (EUR)");
    fixedText.put("FR_TOTAL_AMOUNT","Montant total (EUR)");
    fixedText.put("FR_VAT_INCLU","TOTAL TVA incl.");
    fixedText.put("FR_VAT_EXCLU","TOTAL HTVA");
    fixedText.put("FR_VAT_SQ","TOTAL TVA");
    fixedText.put("FR_EXEMPT_FROM","Exonéré de TVA (article 44 du code de TVA)");
    fixedText.put("FR_PROMO_DISCOUNT","Code promotionnel");
    // Table - NL
    fixedText.put("NL_PRODUCT_DESCRIPTION","Product beschrijving");
    fixedText.put("NL_QUANTITY","Aantal");
    fixedText.put("NL_PRICE_PER_UNIT","Prijs per stuk (EUR)");
    fixedText.put("NL_TOTAL_AMOUNT","Totaal bedrag (EUR)");
    fixedText.put("NL_VAT_INCLU","TOTAAL inclusief btw");
    fixedText.put("NL_VAT_EXCLU","TOTAAL exclusief btw");
    fixedText.put("NL_VAT_SQ","TOTAAL btw");
    fixedText.put("NL_EXEMPT_FROM","Vrijgesteld van btw (artikel 44 van het btwwetboek)");
    fixedText.put("NL_PROMO_DISCOUNT","Promocode");
    
    // FOOTER - EN
    fixedText.put("EN_FOOTER_1","bpost N.V. van publiek recht, MULTI, 1000 Brussel. BTW BE 0 214 596 464, handelsregisternummer: 0214.596.464. RPM Brussel. IBAN BE940000 0000 1414, BIC");
    fixedText.put("EN_FOOTER_2","BPOTBEB1. Algemene Voorwaarden <a href=\"http://www.bpost.be/site/nl/conditions.html\">http://www.bpost.be/site/nl/conditions.html</a>");


    // FOOTER - FR
    fixedText.put("FR_FOOTER_1","bpost, Société Anonyme de Droit Public, MULTI, 1000 Bruxelles. TVA BE 0 214 596 464, Numéro d'entreprise : 0214.596.464. RPM Bruxelles. IBAN BE940000");
    fixedText.put("FR_FOOTER_2","0000 1414, BIC BPOTBEB1. Conditions générales <a href=\"http://www.bpost.be/site/fr/conditions.html\">http://www.bpost.be/site/fr/conditions.html</a>");
    // FOOTER - NL
    fixedText.put("NL_FOOTER_1","bpost N.V. van publiek recht, MULTI, 1000 Brussel. BTW BE 0 214 596 464, handelsregisternummer: 0214.596.464. RPM Brussel. IBAN BE940000 0000 1414, BIC");
    fixedText.put("NL_FOOTER_2","BPOTBEB1. Algemene Voorwaarden <a href=\"http://www.bpost.be/site/nl/conditions.html\">http://www.bpost.be/site/nl/conditions.html</a>");

    }

    private ReportConstants(){}
}
