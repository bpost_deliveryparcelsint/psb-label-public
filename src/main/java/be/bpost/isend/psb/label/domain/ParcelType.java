package be.bpost.isend.psb.label.domain;

public enum ParcelType {

    DOMESTIC_PARCEL("Nationaal Pakket | Paquet National | Nationales Paket"),
    CASH_ON_DELIVERY("Verrekenzending - Contre remboursement - Nachnahmelieferung");

    private String displayName;

    ParcelType(String displayName){
        this.displayName = displayName;
    }
    public String displayName() {
        return displayName;
    }
}
