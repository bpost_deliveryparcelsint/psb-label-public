package be.bpost.isend.psb.label.service.catalog;

import be.bpost.isend.psb.label.domain.Language;
import be.bpost.isend.psb.order.openapi.psb.catalog.PSBCatalogItem;
import be.bpost.isend.psb.order.openapi.psb.catalog.PSBCountryDetails;
import be.bpost.isend.psb.order.openapi.psb.catalog.PSBProductCatalogItems;
import be.bpost.isend.psb.order.openapi.psb.catalog.PSBZone;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class CatalogDataCache {

    private static final Logger logger = LoggerFactory.getLogger(CatalogDataCache.class);

    @Value("${psb.catalog.mock}")
    private boolean useMock;

    private PsbCatalogService catalogService;
    private Map<Language, TranslatedItems> itemsPerLanguage = null;

    public static Map<String,String> legacyShipmentTypesForNero = Map.of(
            "GIFTS","GIFT",
            "GOODS","SALE_OF_GOODS",
            "SAMPLE","COMMERCIAL_SAMPLE",
            "RETURNED_GOODS","SALE_OF_GOODS"
    );

    public static Map<String,String> legacyNonDeliveryInstructionForNero = Map.of(
            "RETURN_TO_SENDER","RETURN_TO_SENDER_NON_PRIORITY"
    );

    @Autowired
    public CatalogDataCache(PsbCatalogService catalogService) {
        this.catalogService = catalogService;
    }

    private void loadCacheLazily() {
        if (itemsPerLanguage != null) {
            return;
        }
        loadCache();
    }

    private void loadCache() {

        Stopwatch stopwatch = Stopwatch.createStarted();
        logger.info("Loading product catalog data into cache...");

        // Catalog items
        itemsPerLanguage = loadTranslationsCache();

        logger.info("Product catalog data cached in {} ms.", stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    private Map<Language, TranslatedItems> loadTranslationsCache() {
        Map<Language, TranslatedItems> newMap = Maps.newHashMap();
        Arrays.stream(Language.values()).forEach(l -> newMap.put(l, loadItemsForLanguage(l)));
        return newMap;
    }

    private TranslatedItems loadItemsForLanguage(Language language) {
        PSBProductCatalogItems catalogItems = catalogService.getItems(language);
        return new TranslatedItems(catalogItems);
    }

    public String getShipmentTypes(String shipmentTypesCode, Language language){
        if (useMock) {
            return shipmentTypesCode;
        }

        loadCacheLazily();
        String actualShipmentTypes = shipmentTypesCode;
        if(legacyShipmentTypesForNero.containsKey(shipmentTypesCode)){
            actualShipmentTypes = legacyShipmentTypesForNero.get(shipmentTypesCode);
        }
        PSBCatalogItem shipmentTypes = itemsPerLanguage.get(language).getShipmentTypes(actualShipmentTypes);
        if (shipmentTypes == null) {
           return null;
        }
        return shipmentTypes.getName();
    }

    public String getNonDeliveryInstructions(String nonDeliveryInstructionsCode, Language language){
        if (useMock) {
            return nonDeliveryInstructionsCode;
        }
        loadCacheLazily();
        String actualNonDeliveryInstructions = nonDeliveryInstructionsCode;
        if(legacyNonDeliveryInstructionForNero.containsKey(nonDeliveryInstructionsCode)){
            actualNonDeliveryInstructions = legacyNonDeliveryInstructionForNero.get(nonDeliveryInstructionsCode);
        }
        PSBCatalogItem nonDeliveryInstructions = itemsPerLanguage.get(language).getNonDeliveryInstructions(actualNonDeliveryInstructions);
        if (nonDeliveryInstructions == null) {
           return null;
        }
        return nonDeliveryInstructions.getName();
    }

    public String getCountryName(String countryCode, Language language) {
        if (useMock) {
            return countryCode;
        }
        loadCacheLazily();
        PSBCountryDetails country = itemsPerLanguage.get(language).getCountry(countryCode);
        if (country == null) {
            return null;
        }
        return country.getName();
    }

    public boolean isDestinationInEuropeanUnion(String countryCode) {
        if (useMock) {
            return true;
        }
        loadCacheLazily();
        PSBCountryDetails country = itemsPerLanguage.get(Language.EN).getCountry(countryCode);
        if (country == null) {
            return false;
        }
        return country.getZone() == PSBZone.EUROPEAN_UNION || country.getZone() == PSBZone.DOMESTIC;
    }

}
