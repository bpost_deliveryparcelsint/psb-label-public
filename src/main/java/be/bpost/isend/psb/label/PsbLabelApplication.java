package be.bpost.isend.psb.label;

import com.google.common.collect.Lists;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PsbLabelApplication {

	public static void main(String[] args) {
		SpringApplication.run(PsbLabelApplication.class, args);
	}

	@Bean
	public OpenAPI customOpenAPI(
			@Value("${doc.application-description}") String appDescription,
			@Value("${doc.application-version}") String appVersion,
			@Value("${doc.server-url}") String serverUrl,
			@Value("${server.servlet.context-path}") String contextPath) {
		return new OpenAPI()
				.servers(Lists.newArrayList(new Server().url(serverUrl + contextPath)))
				.components(new Components().addSecuritySchemes("api-key",
						new SecurityScheme().type(SecurityScheme.Type.APIKEY).in(SecurityScheme.In.HEADER).name("x-api-key")))
				.info(new Info()
						.title("Label Generation API")
						.version(appVersion)
						.description(appDescription));
	}
}
