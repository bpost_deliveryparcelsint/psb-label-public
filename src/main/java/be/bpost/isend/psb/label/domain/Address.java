package be.bpost.isend.psb.label.domain;

import javax.validation.constraints.NotBlank;

public class Address {

    private String firstName;

    private String lastName;

    @NotBlank
    private String street;

    private String streetNumber;

    private String box;

    private String addressOne;

    private String addressTwo;

    @NotBlank
    private String postalCode;

    @NotBlank
    private String city;

    private String country;

    private String pickupPoint;

    private String parcelLocker;

    private String company; // "Business" in case of cn23

    private String phoneNum; // "Tel No" in case of cn23

    private String email;

   /** Getters and Setters*/

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getPhoneNum() { return phoneNum; }

    public void setPhoneNum(String phoneNum) { this.phoneNum = phoneNum; }

    public String getCompany() { return company; }

    public void setCompany(String company) { this.company = company; }

    public String getAddressOne() { return addressOne; }

    public void setAddressOne(String addressOne) { this.addressOne = addressOne; }

    public String getAddressTwo() { return addressTwo; }

    public void setAddressTwo(String addressTwo) { this.addressTwo = addressTwo; }

    public String getPickupPoint() { return pickupPoint; }

    public void setPickupPoint(String pickupPoint) { this.pickupPoint = pickupPoint; }

    public String getParcelLocker() { return parcelLocker; }

    public void setParcelLocker(String parcelLocker) { this.parcelLocker = parcelLocker; }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getBox() {
        return box;
    }

    public void setBox(String box) {
        this.box = box;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() { return country; }

    public void setCountry(String country) {
        this.country = country;
    }
}
