package be.bpost.isend.psb.label.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

public class ProofOfPaymentInput {


    @NotNull
    private String orderNumber;

    @NotNull
    private String paymentMethod;

    @NotNull
    private String date;

    private String cardNumber;

    @NotNull
    @Valid
    private Address sender;

    @NotEmpty
    @Valid
    private List<ProductDetails> productDetails;

    @NotNull
    private Language language;

    private String vatNumber;

    @NotNull
    private BigDecimal vatTotal;

    private BigDecimal vatInclusive; //sum of TOTAL VAT excluded + TOTAL VAT - Promotion code discount

    @NotNull
    private BigDecimal vatExclude;

    private BigDecimal discountAmount;

    private BigDecimal totalAmount; //sum of TOTAL VAT excluded + TOTAL VAT - Promotion code discount

    public String getCardNumber() { return cardNumber; }

    public void setCardNumber(String cardNumber) { this.cardNumber = cardNumber; }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public BigDecimal getVatInclusive() {
        return vatInclusive;
    }

    public void setVatInclusive(BigDecimal vatInclusive) {
        this.vatInclusive = vatInclusive;
    }

    public BigDecimal getVatExclude() {
        return vatExclude;
    }

    public void setVatExclude(BigDecimal vatExclude) {
        this.vatExclude = vatExclude;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getTotalAmount() { return totalAmount; }

    public void setTotalAmount(BigDecimal totalAmount) { this.totalAmount = totalAmount; }

    public Address getSender() { return sender; }

    public void setSender(Address sender) { this.sender = sender; }

    public List<ProductDetails> getProductDetails() { return productDetails; }

    public void setProductDetails(List<ProductDetails> productDetails) { this.productDetails = productDetails; }

    public BigDecimal getVatTotal() { return vatTotal; }

    public void setVatTotal(BigDecimal vatTotal) { this.vatTotal = vatTotal; }

    public Language getLanguage() { return language; }

    public void setLanguage(Language language) { this.language = language; }

    public String getOrderNumber() {  return orderNumber; }

    public void setOrderNumber(String orderNumber) { this.orderNumber = orderNumber; }

    public String getPaymentMethod() { return paymentMethod; }

    public void setPaymentMethod(String paymentMethod) { this.paymentMethod = paymentMethod; }

    public String getDate() { return date; }

    public void setDate(String date) { this.date = date; }
}
