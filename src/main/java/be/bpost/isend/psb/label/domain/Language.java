package be.bpost.isend.psb.label.domain;

public enum Language {

    EN("EN"),
    FR("FR"),
    NL("NL");

    private String lang;

    Language(String lang){
        this.lang = lang;
    }

    public String getLang() {
        return lang;
    }

}
