package be.bpost.isend.psb.label.report;

public class LabelConstants {

    // Label Templates Names
    public static final String MAIN_TEMPLATE = "/mainLabel.htm";
    public static final String DOMESTIC_TEMPLATE = "/domestic_sub_label.html";
    public static final String INTERNATIONAL_TEMPLATE = "/international_sub_label.html";
    public static final String INTERNATIONAL_PUGO_TEMPLATE = "/international_pugo_sub_label.html";
    public static final String PROOF_OF_PAYMENT_TEMPLATE = "/proof_of_payment.html";
    public static final String CN23_TEMPLATE = "/cn23_sub_document.html";

    // PUGO
    public static final String PUGO = "<p>PU<br>GO</p>";

    // Locker
    public static final String LOCKER = "<p style=\"font-size: 16px;padding-top: 13px;\">B247</p>";
    public static final String LOCKER_A6 = "<p style=\"font-size: 15px;padding-top: 10px;\">B247</p>";

    // cod image style
    public static final String COD_A4 = "height: 21px;margin-top: 3px;";
    public static final String COD_A6 = "height: 22px;margin-top: 1px;";

    // Domestic receiver style
    public static final String REC_A4 = "float: left; margin-top: 3px; width: 100%;";
    public static final String REC_A6 = "float: left; margin-top: 0px; width: 100%;";

    // $divStyle
    public static final String DIV_1_STYLE = "margin-right:14.5px";
    public static final String DIV_2_STYLE = "margin-left:14.5px";
    public static final String DIV_3_STYLE = "clear:both;margin-right:14.5px;margin-top: 16px";
    public static final String DIV_4_STYLE = "margin-left:14.5px;margin-top: 16px";
    public static final String DIV_A6_STYLE= "width:100%;";

    // domestic/international css file
    public static final String PARCEL_A4_CSS = "<link type=\"text/css\" rel=\"stylesheet\" href=\"./labelData/print.css\">";
    public static final String PARCEL_A6_CSS = "<link type=\"text/css\" rel=\"stylesheet\" href=\"./labelData/print_a6.css\">";

    // CN23 document
    public static final String CN23_A4_CSS = "<link type=\"text/css\" rel=\"stylesheet\" href=\"./labelData/cn23Document.css\">";
    public static final String CN23_A6_CSS = "<link type=\"text/css\" rel=\"stylesheet\" href=\"./labelData/cn23Document_a6.css\">";

    // International OUT
    public static final String OUT_INTERNATIONAL = "<div class=\"label-international-out\"><p>OUT</p></div>";

    // PDF Style
    public static final String DIV_STYLE = "$divStyle";
    public static final String CSS_FILE = "$csFil";
    public static final String KLANT = "$klant";
    public static final String BLOCK = "block";
    public static final String NONE = "none";
    public static final String REC_COMPANY = "$recCompnay";
    public static final String BR = "<br>";
    public static final String LEFT_BLOCK = "$leftBlock";
    public static final String HIDE_COD = "$hidCod";
    public static final String PICK_UP = "$pckUp";
    public static final String CLOSE_DIV = "</div>";

    public static final String DOMESTIC_COUNTRY = "BELGIË | BELGIQUE | BELGIEN";
    public static final String LABEL_DATA = "$labelData";
    public static final String BAR_CODE_IMAGE = "$barCodeImage";
    public static final String TD_CLOSE = "</td>";

    public static final String EPG_LOGO = "<img src=\"./labelData/epg_logo.png\">";
    public static final String ADDITIONAL_TEXT_FOR_EXPRESS = "<br>ALWAYS ADD 4 COPIES OF THE INVOICE FOR INTERNATIONAL EXPRESS OUT OF THE EU";

    public static final String WEIGHT_LOGO = "<img src=\"./labelData/weight_circle.png\"><p>$maxWeight kg</p>";
    public static final String NO_WEIGHT_LOGO = "<img src=\"./labelData/no_circle.png\"><p></p>";

    private LabelConstants(){}


}
