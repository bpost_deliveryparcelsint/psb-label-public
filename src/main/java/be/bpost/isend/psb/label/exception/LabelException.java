package be.bpost.isend.psb.label.exception;

public class LabelException extends  RuntimeException {

    public LabelException(String message, Exception exception){
        super(message,exception);
    }

    public LabelException(Exception exception) {
        super(exception);
    }
}
