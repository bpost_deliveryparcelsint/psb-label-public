package be.bpost.isend.psb.label.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ParcelLabelInput {

    @NotNull
    private PageFormat pageFormat;

    @NotEmpty
    @Valid
    private List<Parcel> parcels;

    public PageFormat getPageFormat() {
        return pageFormat;
    }

    public void setPageFormat(PageFormat pageFormat) {
        this.pageFormat = pageFormat;
    }

    public List<Parcel> getParcels() { return parcels; }

    public void setParcels(List<Parcel> parcels) { this.parcels = parcels; }
}
