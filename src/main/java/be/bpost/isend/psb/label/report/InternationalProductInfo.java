package be.bpost.isend.psb.label.report;

import be.bpost.isend.psb.label.domain.Category;

public class InternationalProductInfo {
    private final String productLabel;
    private final Category category;
    private final boolean showEPGLogo;
    private final String additionalText;

    public InternationalProductInfo(String productLabel, Category category, boolean showEPGLogo, String additionalText) {
        this.productLabel = productLabel;
        this.category = category;
        this.showEPGLogo = showEPGLogo;
        this.additionalText = additionalText;
    }

    public String getProductLabel() {
        return productLabel;
    }

    public Category getCategory() {
        return category;
    }

    public boolean isShowEPGLogo() {
        return showEPGLogo;
    }

    public String getAdditionalText() {
        return additionalText;
    }
}
