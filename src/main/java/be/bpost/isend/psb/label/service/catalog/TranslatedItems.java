package be.bpost.isend.psb.label.service.catalog;

import be.bpost.isend.psb.order.openapi.psb.catalog.PSBCatalogItem;
import be.bpost.isend.psb.order.openapi.psb.catalog.PSBCountryDetails;
import be.bpost.isend.psb.order.openapi.psb.catalog.PSBProductCatalogItems;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TranslatedItems {

    private Map<String, PSBCatalogItem> shipmentTypes;
    private Map<String, PSBCatalogItem> nonDeliveryInstructions;
    private Map<String, PSBCountryDetails> countriesByCode;

    public TranslatedItems(PSBProductCatalogItems catalogItems) {
        shipmentTypes = buildMap(catalogItems.getShipmentTypes());
        nonDeliveryInstructions = buildMap(catalogItems.getNonDeliveryInstructions());
        countriesByCode = buildCountryMap(catalogItems.getCountries());
    }

    private Map<String, PSBCatalogItem> buildMap(List<PSBCatalogItem> items) {
        return items.stream().collect(Collectors.toMap(PSBCatalogItem::getCode, Function.identity()));
    }

    private Map<String, PSBCountryDetails> buildCountryMap(List<PSBCountryDetails> items) {
        return items.stream().collect(Collectors.toMap(PSBCountryDetails::getCode, Function.identity()));
    }

    public PSBCatalogItem getShipmentTypes(String countryCode) {
        return shipmentTypes.get(countryCode);
    }

    public PSBCatalogItem getNonDeliveryInstructions(String nonDeliveryInstructionsCode) { return nonDeliveryInstructions.get(nonDeliveryInstructionsCode); }

    public PSBCountryDetails getCountry(String countryCode) { return countriesByCode.get(countryCode); }
}
