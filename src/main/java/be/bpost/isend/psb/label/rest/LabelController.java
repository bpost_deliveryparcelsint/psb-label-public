package be.bpost.isend.psb.label.rest;

import be.bpost.isend.psb.label.domain.*;
import be.bpost.isend.psb.label.service.ParcelLabelService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin()
@Tag(name = "label", description = "the label API")
public class LabelController {

    private ParcelLabelService parcelLabelService;

    @Autowired
    public LabelController(ParcelLabelService parcelLabelService) {
        this.parcelLabelService = parcelLabelService;
    }

    @PostMapping(value = "/labels/generate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_PDF_VALUE)
    public  @ResponseBody byte[] generateLabel(@Valid @RequestBody ParcelLabelInput labelInput) {
        return parcelLabelService.generateLabel(labelInput);
    }

    @PostMapping(value = "/proof-of-payment/generate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_PDF_VALUE)
    public  @ResponseBody byte[] generateProofOfPayment(@Valid @RequestBody ProofOfPaymentInput proofOfPaymentInput) {
        return parcelLabelService.generateProofOfPayment(proofOfPaymentInput);
    }

    @PostMapping(value = "/cn23-document/generate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_PDF_VALUE)
    public  @ResponseBody byte[] generateCN23Document(@Valid @RequestBody CN23DocumentInput cn23documentInput) {
        return parcelLabelService.generateCN23Document(cn23documentInput);
    }

    @PostMapping(value = "/labels-with-cn23/generate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_PDF_VALUE)
    public  @ResponseBody byte[] generateParcelWithCn23(@Valid @RequestBody ParcelWithCN23Input parcelWithCN23Input) {
        return parcelLabelService.generateParcelWithCn23(parcelWithCN23Input);
    }


}
