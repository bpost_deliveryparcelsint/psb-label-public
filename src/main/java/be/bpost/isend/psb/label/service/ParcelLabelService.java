package be.bpost.isend.psb.label.service;

import be.bpost.isend.psb.label.domain.*;
import be.bpost.isend.psb.label.exception.LabelException;
import be.bpost.isend.psb.label.report.LabelUtility;
import be.bpost.isend.psb.label.report.PDFGenerator;
import com.google.common.base.Stopwatch;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.output.OutputException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class ParcelLabelService {

    private static final Logger logger = LoggerFactory.getLogger(ParcelLabelService.class);

    private LabelUtility labelUtility;

    @Autowired
    public ParcelLabelService(LabelUtility labelUtility) {
        this.labelUtility = labelUtility;
    }

    public byte[] generateLabel(ParcelLabelInput labelInput) {

        try {
            Stopwatch stopwatch = Stopwatch.createStarted();
            logger.info("Generating Labels for {} parcels...", labelInput.getParcels().size());

            List<String> htmlStr = labelUtility.setParcelLabelValues(labelInput);

            byte[] returnPdf = PDFGenerator.createPdf(htmlStr, labelInput.getPageFormat().getPageSize(),true);

            logger.info("Generated Labels for {} parcels in {} ms.", labelInput.getParcels().size(), stopwatch.elapsed(TimeUnit.MILLISECONDS));

            return returnPdf;
        } catch (IOException | BarcodeException | OutputException e) {
            throw new LabelException("Exception when generating label: " + e, e);
        }
    }

    public byte[] generateProofOfPayment(ProofOfPaymentInput proofOfPaymentInput){
        try {
            Stopwatch stopwatch = Stopwatch.createStarted();
            logger.info("Proof of payment Start");

            List<String> htmlStr = labelUtility.setProofOfPaymentValues(proofOfPaymentInput);
            byte[] returnPdf = PDFGenerator.createPdf(htmlStr, PageFormat.A4.getPageSize(),false);
            logger.info("Proof of payment document End in {} ms.", stopwatch.elapsed(TimeUnit.MILLISECONDS));
            return returnPdf;

        } catch (IOException e) {
            throw new LabelException("Exception when generating Proof of payment: " + e, e);
        }
    }


    public byte[] generateCN23Document(CN23DocumentInput cn23documentInput) {
        try {
            Stopwatch stopwatch = Stopwatch.createStarted();
            logger.info("CN23 document Start");

            List<String> htmlStr = labelUtility.setCN23DocumentValues(cn23documentInput);
            logger.debug("Generated HTML for CN23 in {} ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));
            byte[] returnPdf = PDFGenerator.createPdf(htmlStr, cn23documentInput.getPageFormat().getPageSize(),true);
            logger.info("CN23  document End in {} ms.", stopwatch.elapsed(TimeUnit.MILLISECONDS));
            return returnPdf;

        } catch (IOException | BarcodeException | OutputException e) {
            throw new LabelException("Exception when generating CN23  document : " + e, e);
        }
    }

    public byte[] generateParcelWithCn23(ParcelWithCN23Input parcelWithCN23Input) {
        try {
            Stopwatch stopwatch = Stopwatch.createStarted();
            logger.info("Parcel with CN-23 document Start");

            List<String> htmlStr = labelUtility.setParcelWithCN23Values(parcelWithCN23Input);
            byte[] returnPdf = PDFGenerator.createPdf(htmlStr, parcelWithCN23Input.getPageFormat().getPageSize(),true);
            logger.info("Parcel with CN23 document End in {} ms.", stopwatch.elapsed(TimeUnit.MILLISECONDS));
            return returnPdf;
        }catch (IOException | BarcodeException | OutputException e){
            throw new LabelException("Exception when generating label: " + e, e);
        }
    }
}
