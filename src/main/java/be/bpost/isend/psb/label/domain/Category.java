package be.bpost.isend.psb.label.domain;

public enum Category {
    MAIL("U"),
    PARCEL("P"),
    REGISTERED_LETTER("R");

    private final String abbreviation;

    Category(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
}
