package be.bpost.isend.psb.label.domain;

import be.bpost.isend.psb.label.domain.validation.NonDeliveryInstruction;
import be.bpost.isend.psb.label.domain.validation.ShipmentType;
import com.google.common.base.Strings;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

public class CN23 {

    @NotBlank
    private String barcode;

    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @NotNull
    @Valid
    private Address sender;

    @NotNull
    @Valid
    private Address receiver;

    @Valid
    private List<Item> items;

    @NotNull
    private BigDecimal totalValue;

    @NotNull
    private BigDecimal totalGrossWeight;

    private BigDecimal postalCharges;

    @NotNull
    @ShipmentType
    private String shipmentType;

    private String otherShipmentType; // For Other Shipment Type

    @NotBlank
    @Length(min=1, max=50)
    private String contentDescription;

    @NotNull
    @NonDeliveryInstruction
    private String nonDeliveryInstruction;

    @NotBlank
    private String date;

    /** Getters and Setters*/

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Address getSender() {
        return sender;
    }

    public void setSender(Address sender) {
        this.sender = sender;
    }

    public Address getReceiver() {
        return receiver;
    }

    public void setReceiver(Address receiver) {
        this.receiver = receiver;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }

    public BigDecimal getTotalGrossWeight() {
        return totalGrossWeight;
    }

    public void setTotalGrossWeight(BigDecimal totalGrossWeight) {
        this.totalGrossWeight = totalGrossWeight;
    }

    public BigDecimal getPostalCharges() {
        return postalCharges;
    }

    public void setPostalCharges(BigDecimal postalCharges) {
        this.postalCharges = postalCharges;
    }

    public String getShipmentType() { return shipmentType; }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String getOtherShipmentType() {
        return Strings.isNullOrEmpty(otherShipmentType) ? "" : otherShipmentType;
    }

    public void setOtherShipmentType(String otherShipmentType) {
        this.otherShipmentType = otherShipmentType;
    }

    public String getContentDescription() {
        return contentDescription;
    }

    public void setContentDescription(String contentDescription) {
        this.contentDescription = contentDescription;
    }

    public String getNonDeliveryInstruction() {  return nonDeliveryInstruction; }

    public void setNonDeliveryInstruction(String nonDeliveryInstruction) { this.nonDeliveryInstruction = nonDeliveryInstruction; }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
