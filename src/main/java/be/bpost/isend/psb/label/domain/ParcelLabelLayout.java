package be.bpost.isend.psb.label.domain;

public enum ParcelLabelLayout {
    PREPAID_DOMESTIC,
    PREPAID_INTL,
    PREPAID_INTL_PUGO,
    CONTRACTUAL_INTL
}
