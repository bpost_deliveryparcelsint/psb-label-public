package be.bpost.isend.psb.label.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class LabelValidationException extends  RuntimeException{

    public LabelValidationException(String message){super(message);}
}
