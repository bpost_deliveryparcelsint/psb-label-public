package be.bpost.isend.psb.label.report;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.WriterProperties;
import com.itextpdf.kernel.utils.PdfMerger;
import com.itextpdf.layout.font.FontProvider;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public class PDFGenerator {

    public static byte[] createPdf(List<String> htmlPages, PageSize pageSize, boolean rotate) throws IOException {

        ConverterProperties properties = new ConverterProperties();

        // Register classpath protocol handler to be able to load HTML resources from class patch
        org.apache.catalina.webresources.TomcatURLStreamHandlerFactory.register();
        properties.setBaseUri("classpath:/");

        FontProvider fontProvider = new DefaultFontProvider(true, false, false);
        properties.setFontProvider(fontProvider);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        WriterProperties writerProperties = new WriterProperties();
        writerProperties.useSmartMode();
        PdfDocument pdf = new PdfDocument(new PdfWriter(byteArrayOutputStream, writerProperties));
        PdfMerger merger = new PdfMerger(pdf);

        for (String htmlPage : htmlPages) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PdfDocument temp = new PdfDocument(new PdfWriter(baos, writerProperties));
            if (rotate) {
                temp.setDefaultPageSize(pageSize.rotate()); // Page Size and Orientation
            } else {
                temp.setDefaultPageSize(pageSize); // Page Size and Orientation
            }
            HtmlConverter.convertToPdf(htmlPage, temp, properties);
            temp = new PdfDocument(new PdfReader(new ByteArrayInputStream(baos.toByteArray())));
            merger.merge(temp, 1, temp.getNumberOfPages());
            temp.close();
        }
        pdf.close();
        byteArrayOutputStream.close();

        return byteArrayOutputStream.toByteArray();
    }

    private PDFGenerator(){ }
}
