package be.bpost.isend.psb.label.domain;

import be.bpost.isend.psb.label.exception.LabelValidationException;
import com.google.common.base.Strings;

public class InternationalAddress {


    private String contentDescription;
    private String accountId;
    private String shipmentType;
    private String nonDeliveryInstruction;
    private String productName;
    private boolean eParcelGroupLabel;

    public String getNonDeliveryInstruction() {
        if(Strings.isNullOrEmpty(nonDeliveryInstruction))
            throw new LabelValidationException("Non delivery instruction is missing");
        return nonDeliveryInstruction;
    }

    public void setNonDeliveryInstruction(String nonDeliveryInstruction) { this.nonDeliveryInstruction = nonDeliveryInstruction; }

    public String getContentDescription() {
        return contentDescription;
    }

    public void setContentDescription(String contentDescription) {
        this.contentDescription = contentDescription;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getShipmentType() {
        if(Strings.isNullOrEmpty(shipmentType))
            throw new LabelValidationException("Shipment Type is missing");
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String getProductName() { return productName; }

    public void setProductName(String productName) { this.productName = productName;}

    public boolean iseParcelGroupLabel() { return eParcelGroupLabel;}

    public void seteParcelGroupLabel(boolean eParcelGroupLabel) { this.eParcelGroupLabel = eParcelGroupLabel;}

}
