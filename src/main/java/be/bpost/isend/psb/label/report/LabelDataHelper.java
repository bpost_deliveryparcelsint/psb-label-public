package be.bpost.isend.psb.label.report;

import be.bpost.isend.psb.label.JoinUtils;
import be.bpost.isend.psb.label.domain.*;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.output.OutputException;
import org.apache.commons.lang3.StringUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.Base64;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static be.bpost.isend.psb.label.report.LabelConstants.*;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;

public class LabelDataHelper {
    static String formatBox(String box) {
        if (StringUtils.isBlank(box)) {
            return "";
        }
        return " / " + box;
    }

    public static boolean isNumeric(String str) {
        if (str == null || str.isBlank()) {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    static String formatAmount(BigDecimal amount) {
        if(null == amount){
            return "0,00";
        }
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator(',');
        return new DecimalFormat("###0.00", decimalFormatSymbols).format(amount);
    }

    static String formatAmountInt(Integer amount) {
        if(null == amount){
            return "0";
        }
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator(',');
        return new DecimalFormat("###0.00", decimalFormatSymbols).format(amount);
    }

    static String formatCodBarCode(String codBarCode){
        String[] arr = codBarCode.split("");
        StringBuilder formattedBarCode = new StringBuilder();
        formattedBarCode.append(arr[0]);
        for (int i =1 ; i <= arr.length-1 ; i++) {
            formattedBarCode.append("\t").append(arr[i]);
        }
        return formattedBarCode.toString();
    }

    public static String getBarCodeImage(String barCode) throws OutputException, BarcodeException, IOException {

        Barcode barcode = null;

        if(isNumeric(barCode)){
            barcode = BarcodeFactory.createCode128C(barCode);
        }else {
            barcode = BarcodeFactory.createCode128(barCode);
        }
        // 44,257
        barcode.setBarHeight(34);
        barcode.setBarWidth(68);
        barcode.setDrawingText(false);

        BufferedImage image =  BarcodeImageHandler.getImage(barcode);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(image, "png", baos);
        byte[] bytes = baos.toByteArray();

        return Base64.getEncoder().encodeToString(bytes);
    }
    public static String formatValue(BigDecimal amount) {
        if(null == amount){
            return "0";
        }
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(amount);
    }

    public static String truncateCompanyName(String company){

        if(Strings.isNullOrEmpty(company)){
            return "";
        }

        return (company.length() > 20 ? company.substring(0,20) : company)+"<br>";
    }

    public static String setCodDetails(PageFormat pageFormat, Parcel parcel, String label, Address receiver) throws OutputException, BarcodeException, IOException {
        label = label.replace("$codBarCode", getBarCodeImage(parcel.getCodBarcode()));
        label = label.replace("$amount", formatAmount(parcel.getCodAmount()));
        label = label.replace("$bankAcc", formatCODBankAccount(parcel.getCodBankAccount(),4));
        label = label.replace("$cdbar", formatCodBarCode(parcel.getCodBarcode()));
        label = label.replace(LEFT_BLOCK, "");
        label = label.replace(KLANT, "");
        label = label.replace("$via", BR);
        label = label.replace(HIDE_COD, BLOCK);
        label = label.replace("$cdSty", pageFormat.equals(PageFormat.A4) ? COD_A4: COD_A6);
        label = label.replace(PICK_UP, "");
        label = label.replace(REC_COMPANY, Strings.isNullOrEmpty(receiver.getCompany()) ? "" : receiver.getCompany() + BR);
        return label;
    }

    public static String getDivStyle(int divNumber){

        switch (divNumber){
            case 1 : return LabelConstants.DIV_1_STYLE;
            case 2 : return LabelConstants.DIV_2_STYLE;
            case 3 : return LabelConstants.DIV_3_STYLE;
            case 4 : return LabelConstants.DIV_4_STYLE;
            default: return LabelConstants.DIV_A6_STYLE;
        }

    }

    public static String formatCODBankAccount(String bankAccountNum, int fixLength){
        Iterable<String> result = Splitter.fixedLength(fixLength).split(bankAccountNum.trim());
        return removeJunk(Iterables.toString(result));
    }

    private static String removeJunk(String value){
        return value.replace("[","").replace("]","").replace(",","");
    }

    public static String setSenderAddress(String label, Address sender){
        label = label.replace("$senderFirstName", defaultIfEmpty(sender.getFirstName(),""));
        label = label.replace("$senderLastName", defaultIfEmpty(sender.getLastName(),""));
        label = label.replace("$senCompnay", LabelDataHelper.truncateCompanyName(sender.getCompany()));

        label = label.replace("$senderStreet", sender.getStreet());
        label = label.replace("$sendrStrtNum", defaultIfEmpty(sender.getStreetNumber(),""));
        label = label.replace("$senderBox", LabelDataHelper.formatBox(sender.getBox()) + BR);

        label = label.replace("$senderPostalCode", sender.getPostalCode());
        label = label.replace("$senderCity", sender.getCity());
        // For International
        label = label.replace("$senderPhoneNumber", formatWithDelimiter(sender.getPhoneNum(),sender.getEmail()));
        label = label.replace("$senderEmail", defaultIfEmpty(sender.getEmail(),""));
        return label;
    }

    public static String setReceiverAddress(String label, Address receiver, PageFormat pageFormat, ParcelLabelLayout parcelLabelLayout) {
        label = label.replace("$receiverFirstName", defaultIfEmpty(receiver.getFirstName(),""));
        label = label.replace("$receiverLastName", defaultIfEmpty(receiver.getLastName(),""));
        label = label.replace("$recSty", pageFormat.equals(PageFormat.A4) ? REC_A4 : REC_A6);

        if (parcelLabelLayout.equals(ParcelLabelLayout.PREPAID_DOMESTIC) && !Strings.isNullOrEmpty(receiver.getParcelLocker())) {
            label = label.replace("$receiverStreet", "");
            label = label.replace("$recerStrtNum", "");
            label = label.replace("$receiverBox", "");
        }else{
            label = label.replace("$receiverStreet", receiver.getStreet());
            label = label.replace("$recerStrtNum", defaultIfEmpty(receiver.getStreetNumber(),""));
            label = label.replace("$receiverBox", LabelDataHelper.formatBox(receiver.getBox()) + BR);
        }

        label = label.replace("$receiverPostalCode", receiver.getPostalCode());
        label = label.replace("$receiverCity", receiver.getCity());
        return label;
    }

    public static String setParcelInfo(String label, Parcel parcel){
        label = label.replace("$barNumber", parcel.getBarcode());
        label = label.replace("$validityDate", parcel.getValidityDate());
        if(parcel.getLabelLayout().equals(ParcelLabelLayout.CONTRACTUAL_INTL)){
            label = label.replace("$showWeight", NO_WEIGHT_LOGO);
        }else{
            label = label.replace("$showWeight", WEIGHT_LOGO);
        }
        label = label.replace("$maxWeight", parcel.getMaxWeight().toString());
        if(parcel.getLabelLayout().equals(ParcelLabelLayout.PREPAID_INTL_PUGO)){
            label = label.replace("$parcelType", "");
            label = label.replace("$senderHeading", LabelTitle.SENDER.getInternational());
            label = label.replace("$counCodeSty", BLOCK);
        }else{
            label = label.replace("$parcelType", parcel.getParcelType().displayName());
            label = label.replace("$senderHeading", LabelTitle.SENDER.getDomestic());
            label = label.replace("$receiverCountry", DOMESTIC_COUNTRY);
            label = label.replace("$senderCoun", DOMESTIC_COUNTRY);
            label = label.replace("$phoneNum", "");
            label = label.replace("$internationalOut", "");
            label = label.replace("$counCodeSty", NONE);
        }

        if (parcel.isCaptureSignature()) {
            label = label.replace("$showExclamaitaion",BLOCK);
        } else {
            label = label.replace("$showExclamaitaion",NONE);
        }
        if (parcel.isExtraWeight()){
            label = label.replace("$showHideExtraWicht",BLOCK);
        }else{
            label = label.replace("$showHideExtraWicht",NONE);
        }

        return label;
    }

    public static String setFullName(Address address, int len){
        String fullName = JoinUtils.joinWithSpaces(defaultIfEmpty(address.getFirstName(),""),defaultIfEmpty(address.getLastName(),""));
        if(Strings.isNullOrEmpty(fullName)) return "";
        if(fullName.length()<=len){
            return fullName;
        }
        return fullName.substring(0,len);
    }

    public static String setFullAddress(Address address, int len){
        String fullAddress = String.join(" ",address.getStreet(),defaultIfEmpty(address.getStreetNumber(),""),formatBox(address.getBox()));
        if(fullAddress.length()<=len){
            return fullAddress;
        }
        return fullAddress.substring(0,len);
    }

    public static String truncateData(String value, int len){
        if(Strings.isNullOrEmpty(value)){
            return "";
        }
        if(value.length()<=len){
            return value;
        }
        return value.substring(0,len);
    }

    public static String formatWithDelimiter(String add1, String add2){
        return Stream.of(add1, add2)
                .filter(Objects::nonNull)
                .filter(Predicate.not(String::isBlank))
                .collect(Collectors.joining(" - "));
    }

    public static String formatContentDescription(String contentDescription){
        if(Strings.isNullOrEmpty(contentDescription)) return "";

        if(contentDescription.length()>25){
            return removeJunk(Arrays.toString(contentDescription.split("(?<=\\G.{25})")));
        }
        return contentDescription;
    }
    private LabelDataHelper(){}
}
