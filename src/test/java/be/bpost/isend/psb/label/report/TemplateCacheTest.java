package be.bpost.isend.psb.label.report;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TemplateCacheTest {

    @Test
    void testCachingOfTemplates() {

        TemplateCache templateCache = new TemplateCache();
        templateCache.loadHtmlTemplates();
        for (HtmlTemplate template : HtmlTemplate.values()) {
            assertThat(templateCache.getHtmlTemplate(template)).isNotNull();
        }
        assertThat(templateCache.getHtmlTemplate(HtmlTemplate.MAIN_TEMPLATE)).startsWith("<!DOCTYPE html PUBLIC")
            .contains("<html>");
    }
}