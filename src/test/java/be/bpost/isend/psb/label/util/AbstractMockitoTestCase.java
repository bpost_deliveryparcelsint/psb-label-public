package be.bpost.isend.psb.label.util;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockitoAnnotations;

public class AbstractMockitoTestCase {

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }
}
