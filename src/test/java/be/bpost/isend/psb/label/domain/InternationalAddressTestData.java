package be.bpost.isend.psb.label.domain;

public class InternationalAddressTestData {

    public static InternationalAddress internationalAddress(){

        InternationalAddress internationalAddress =new InternationalAddress();
        internationalAddress.setAccountId("999007");
        internationalAddress.setContentDescription("Some content description");
        internationalAddress.setShipmentType("DOCUMENTS");
        internationalAddress.setNonDeliveryInstruction("TREAT_AS_ABANDONED");
        internationalAddress.setProductName("bpack Wordl business");
        internationalAddress.seteParcelGroupLabel(true);
        return internationalAddress;

    }
}
