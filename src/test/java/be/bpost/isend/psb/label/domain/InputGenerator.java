package be.bpost.isend.psb.label.domain;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InputGenerator {

    private static final Logger logger = LoggerFactory.getLogger(InputGenerator.class);

    private PageFormat pageFormat;
    private ParcelLabelLayout labelLayout = ParcelLabelLayout.PREPAID_DOMESTIC;



    public InputGenerator(PageFormat pageFormat, ParcelLabelLayout labelLayout) {
        this.pageFormat = pageFormat;
        this.labelLayout = labelLayout;
    }

    public InputGenerator(PageFormat pageFormat) {
        this.pageFormat = pageFormat;

    }

    public CN23DocumentInput generateCN23DocumentInput(int numberOfPages){
        CN23DocumentInput input = new CN23DocumentInput();
        input.setPageFormat(pageFormat);
        List<CN23> cn23 = Lists.newArrayList();
        for (int i = 0; i < numberOfPages; i++) {
            cn23.add(generateCN23(i+1));
        }
        input.setCn23(cn23);
        return input;
    }

    private CN23 generateCN23(int i){

        CN23 cn23 = new CN23();
        cn23.setBarcode("3232000000000000" + StringUtils.leftPad(String.valueOf(i), 2, '0'));
        cn23.setTotalValue(new BigDecimal(100));
        cn23.setTotalGrossWeight(new BigDecimal(111));
        cn23.setPostalCharges(new BigDecimal(222));
        cn23.setShipmentType("DOCUMENTS");
        cn23.setOtherShipmentType("Fruits");
        cn23.setContentDescription("Contest desd");
        cn23.setNonDeliveryInstruction("TREAT_AS_ABANDONED");
        cn23.setDate("11/22/3333");

        List<Item> itemList = new ArrayList<>();
        for(int j = 0 ; j <=2 ; j++){
            Item item = new Item();
            item.setItemDescription("Item Desc"+j);
            item.setCountry("BE");
            item.setHsTariff("1");
            item.setQuantity(2);
            item.setWeight(new BigDecimal(1));
            item.setValue(new BigDecimal(1));
            itemList.add(item);
        }
        cn23.setItems(itemList);


        Address sender = new Address();
        sender.setFirstName("SenderFirst" + i);
        sender.setLastName("SenderLast" + i);
        sender.setStreet("Sender Street" + i);
        sender.setStreetNumber("1" + i);
        sender.setCity("SenderCity" + i);
        sender.setBox("A" + i);
        sender.setPostalCode("100"+i);
        sender.setCompany("Company"+i);
        sender.setPhoneNum("1234567"+i);
        sender.setCountry("BE");
        cn23.setSender(sender);

        Address receiver = new Address();
        receiver.setFirstName("ReceiverFirst" + i);
        receiver.setLastName("ReceiverLast" + i);
        receiver.setStreet("Receiver Street" + i);
        receiver.setStreetNumber("2" + i);
        receiver.setCity("ReceiverCity" + i);
        receiver.setBox("B" + i);
        receiver.setPostalCode("200"+i);
        receiver.setCompany("Company"+i);
        receiver.setPhoneNum("1234567"+i);
        receiver.setCountry("BE");
        cn23.setReceiver(receiver);

        return cn23;


    }


    public ParcelLabelInput generateParcelLabelInput(int numberOfParcels) {

        ParcelLabelInput input = new ParcelLabelInput();
        input.setPageFormat(pageFormat);
        List<Parcel> parcels = Lists.newArrayList();
        for (int i = 0; i < numberOfParcels; i++) {
            parcels.add(generateParcel(i+1));
        }
        input.setParcels(parcels);
        return input;
    }

    public ParcelWithCN23Input generateParcelWithCN23Input(int numberOfParcels) {

        ParcelWithCN23Input input = new ParcelWithCN23Input();
        List<ParcelWithCN23> parcelWithCN23List = Lists.newArrayList();
        for (int i = 0; i < numberOfParcels; i++) {
            ParcelWithCN23 parcelWithCN23 = new ParcelWithCN23();
            parcelWithCN23.setCn23(generateCN23(1));
            parcelWithCN23.setParcel(ParcelTestData.contractualInternational());
            parcelWithCN23List.add(parcelWithCN23);
        }
        input.setParcelWithCN23(parcelWithCN23List);
        return input;
    }

    private Parcel generateParcel(int i) {

        Parcel parcel = new Parcel();
        parcel.setBarcode("3232000000000000" + StringUtils.leftPad(String.valueOf(i), 2, '0'));
        parcel.setExtraWeight(false);
        parcel.setCaptureSignature(false);
        parcel.setValidityDate("29/07/2020");
        parcel.setMaxWeight(i % 5 + 1);
        parcel.setParcelType(ParcelType.DOMESTIC_PARCEL);

        Address sender = new Address();
        sender.setFirstName("SenderFirst" + i);
        sender.setLastName("SenderLast" + i);
        sender.setStreet("Sender Street" + i);
        sender.setStreetNumber("1" + i);
        sender.setCity("SenderCity" + i);
        sender.setBox("A" + i);
        sender.setPostalCode("100"+i);
        parcel.setSender(sender);

        Address receiver = new Address();
        receiver.setFirstName("ReceiverFirst" + i);
        receiver.setLastName("ReceiverLast" + i);
        receiver.setStreet("Receiver Street" + i);
        receiver.setStreetNumber("2" + i);
        receiver.setCity("ReceiverCity" + i);
        receiver.setBox("B" + i);
        receiver.setPostalCode("200"+i);
        parcel.setReceiver(receiver);

        return parcel;
    }


    public static void main(String[] args) throws Exception {
        //generateProofOfPaymentJson();
        //generateParcelJson();
        //generateCN23Json();
        generateParcelWithCN23Json();
    }

    static void generateParcelWithCN23Json() throws JsonProcessingException {
        InputGenerator generator = new InputGenerator(PageFormat.A4, ParcelLabelLayout.CONTRACTUAL_INTL);
        ParcelWithCN23Input parcelLabelInput = generator.generateParcelWithCN23Input(1);
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        logger.info("Result: \n" + mapper.writeValueAsString(parcelLabelInput));
    }

    static void generateParcelJson() throws JsonProcessingException {
        InputGenerator generator = new InputGenerator(PageFormat.A4, ParcelLabelLayout.PREPAID_INTL);
        ParcelLabelInput parcelLabelInput = generator.generateParcelLabelInput(1);
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        logger.info("Result: \n" + mapper.writeValueAsString(parcelLabelInput));
    }

    static void generateCN23Json() throws JsonProcessingException {
        InputGenerator generator = new InputGenerator(PageFormat.A4);
        CN23DocumentInput cn23DocumentInput = generator.generateCN23DocumentInput(7);
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        logger.info("Result: \n" + mapper.writeValueAsString(cn23DocumentInput));
    }

    static void generateProofOfPaymentJson() throws JsonProcessingException {
        ProofOfPaymentInput proofOfPaymentInput = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        logger.info("Result: \n" + mapper.writeValueAsString(proofOfPaymentInput));
    }

}
