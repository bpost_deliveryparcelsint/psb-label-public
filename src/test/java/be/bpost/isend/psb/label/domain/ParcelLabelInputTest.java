package be.bpost.isend.psb.label.domain;

import be.bpost.isend.psb.label.BeanPropertyTester;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ParcelLabelInputTest {

    @Test
    public void testPageFormat() {
        ParcelLabelInput input = ParcelLabelInputTestData.oneDomesticParcel();
        BeanPropertyTester.propertyTester(input, "pageFormat", PageFormat.class)
                .propertyIsMandatory()
                .validate();
    }

    @Test
    public void testLabelLayout() {
        Parcel input = validParcel();
        BeanPropertyTester.propertyTester(input, "labelLayout", ParcelLabelLayout.class)
                .propertyIsMandatory()
                .validate();
    }

    @Test
    public void testParcels() {

        ParcelLabelInput input = ParcelLabelInputTestData.oneDomesticParcel();
        Parcel invalidParcel = new Parcel();
        BeanPropertyTester.propertyTester(input, "parcels", List.class)
                .propertyIsMandatory()
                .withInvalidValues(new ArrayList(), List.of(invalidParcel))
                .validate();
    }

    @Test
    public void testParcelBarcode() {
        Parcel parcel = validParcel();
        BeanPropertyTester.propertyTester(parcel, "barcode", String.class)
                .propertyIsMandatory()
                .withInvalidValues("", " ")
                .withValidValues("BARCODE")
                .validate();
    }

    @Test
    public void testParcelMaxWeight() {
        Parcel parcel = validParcel();
        BeanPropertyTester.propertyTester(parcel, "maxWeight", Integer.class)
                .propertyIsMandatory()
                .withInvalidValues()
                .withValidValues(12)
                .validate();
    }

    @Test
    public void testParcelExtraWeight() {
        Parcel parcel = validParcel();
        BeanPropertyTester.propertyTester(parcel, "extraWeight", Boolean.class)
                .withValidValues(true)
                .validate();
    }

    @Test
    public void testParcelCaptureSignature() {
        Parcel parcel = validParcel();
        BeanPropertyTester.propertyTester(parcel, "captureSignature", Boolean.class)
                .withValidValues(true)
                .validate();
    }

    @Test
    public void testParcelCodBarcode() {
        Parcel parcel = validParcel();
        BeanPropertyTester.propertyTester(parcel, "codBarcode", String.class)
                .withValidValues("CODBARCODE")
                .validate();
    }

    @Test
    public void testParcelCodAmount() {
        Parcel parcel = validParcel();
        BeanPropertyTester.propertyTester(parcel, "codAmount;", BigDecimal.class)
                .withValidValues()
                .validate();
    }

    @Test
    public void testParcelCodBankAccount() {
        Parcel parcel = validParcel();
        BeanPropertyTester.propertyTester(parcel, "codBankAccount", String.class)
                .withValidValues("B1234")
                .validate();
    }

    @Test
    public void testParcelValidityDate() {
        Parcel parcel = validParcel();
        BeanPropertyTester.propertyTester(parcel, "validityDate", String.class)
                .withValidValues("23/10/2020")
                .validate();
    }

    @Test
    public void testParcelSender() {
        Parcel parcel = validParcel();
        BeanPropertyTester.propertyTester(parcel, "sender", Address.class)
                .propertyIsMandatory()
                .withInvalidValues()
                .withValidValues(parcel.getSender())
                .validate();
    }

    @Test
    public void testParcelReceiver() {
        Parcel parcel = validParcel();
        BeanPropertyTester.propertyTester(parcel, "receiver", Address.class)
                .propertyIsMandatory()
                .withInvalidValues()
                .withValidValues(parcel.getReceiver())
                .validate();
    }



    @Test
    public void testParcelType() {
        ParcelLabelInput input = ParcelLabelInputTestData.oneDomesticParcel();
        BeanPropertyTester.propertyTester(input, "parcelType", ParcelType.class)
                .validate();
    }


    private Parcel validParcel() {
        return ParcelLabelInputTestData.oneDomesticParcel().getParcels().get(0);
    }

}