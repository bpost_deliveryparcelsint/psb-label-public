package be.bpost.isend.psb.label;

import be.bpost.isend.psb.label.domain.*;
import be.bpost.isend.psb.label.service.catalog.CatalogDataCache;
import be.bpost.isend.psb.order.openapi.psb.catalog.PSBProductCatalogItems;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class PsbLabelApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CatalogDataCache catalogDataCache;

    @BeforeEach
    public void configureCatalogData() {

        when(catalogDataCache.getShipmentTypes("OTHER",Language.EN)).thenReturn("Other");
        when(catalogDataCache.getShipmentTypes("COMMERCIAL_SAMPLE",Language.EN)).thenReturn("Commercial sample");
        when(catalogDataCache.getShipmentTypes("SALE_OF_GOODS",Language.EN)).thenReturn("Sale of goods");
        when(catalogDataCache.getShipmentTypes("GIFT",Language.EN)).thenReturn("Gift");
        when(catalogDataCache.getShipmentTypes("DOCUMENTS",Language.EN)).thenReturn("Documents");

        when(catalogDataCache.getNonDeliveryInstructions("TREAT_AS_ABANDONED",Language.EN)).thenReturn("Treat as abandoned");
        when(catalogDataCache.getNonDeliveryInstructions("RETURN_TO_SENDER_PRIORITY",Language.EN)).thenReturn("Return to sender (priority)");
        when(catalogDataCache.getNonDeliveryInstructions("RETURN_TO_SENDER_NON_PRIORITY",Language.EN)).thenReturn("Return to sender (non priority)");

        when(catalogDataCache.getCountryName("BE", Language.EN)).thenReturn("Belgium");
        when(catalogDataCache.getCountryName("FR", Language.EN)).thenReturn("France");
        when(catalogDataCache.getCountryName("CN", Language.EN)).thenReturn("China");
        when(catalogDataCache.getCountryName(
                argThat(countryCode -> !List.of("BE", "FR", "CN").contains(countryCode)),
                eq(Language.EN))
        ).thenThrow(new IllegalArgumentException("Invalid country code!"));

        when(catalogDataCache.isDestinationInEuropeanUnion("BE")).thenReturn(false);
        when(catalogDataCache.isDestinationInEuropeanUnion("FR")).thenReturn(true);
        when(catalogDataCache.isDestinationInEuropeanUnion("CN")).thenReturn(false);
    }

    @Test
    void testCreateLabels() throws Exception {

        ParcelLabelInput input = ParcelLabelInputTestData.oneDomesticParcel();
        byte[] responseBody = mockMvc.perform(
                post("/labels/generate")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(input))
        ).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_PDF))
                .andReturn().getResponse().getContentAsByteArray();

        // Check that response body is a PDF file
        assertThat(isPDF(responseBody)).isTrue();
    }

    @Test
    void testInvalidRequestForCreateLabels() throws Exception {

        ParcelLabelInput input = ParcelLabelInputTestData.oneDomesticParcel();
        input.setPageFormat(null); // page format is required
        mockMvc.perform(
                post("/labels/generate")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(input))
        ).andExpect(status().isBadRequest())
                .andDo(print());
    }


    private boolean isPDF(byte[] binaryContents) {

        // PDF files start with "%PDF" (hex 25 50 44 46)
        if (binaryContents.length < 4) {
            // not enough bytes to check for magic number
            return false;
        }
        return (binaryContents[0] == 0x25)
                && (binaryContents[1] == 0x50)
                && (binaryContents[2] == 0x44)
                && (binaryContents[3] == 0x46);
    }

    @Test
    void testProofOfPayment() throws Exception {

        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        byte[] responseBody = mockMvc.perform(
                post("/proof-of-payment/generate")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(input))
        ).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_PDF))
                .andReturn().getResponse().getContentAsByteArray();

        // Check that response body is a PDF file
        assertThat(isPDF(responseBody)).isTrue();
    }

    @Test
    void testCN23Document() throws Exception {

        CN23DocumentInput input = CN23DocumentInputTestData.cn23DocumentInputData();
        byte[] responseBody = mockMvc.perform(
                post("/cn23-document/generate")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(input))
        ).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_PDF))
                .andReturn().getResponse().getContentAsByteArray();

        assertThat(isPDF(responseBody)).isTrue();
    }

    @Test
    void testInternationalPugo() throws Exception {

        ParcelLabelInput input = ParcelLabelInputTestData.internationalPugo();
        byte[] responseBody = mockMvc.perform(
                post("/labels/generate")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(input))
        ).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_PDF))
                .andReturn().getResponse().getContentAsByteArray();

        assertThat(isPDF(responseBody)).isTrue();
    }

    @Test
    void testDomesticCodLabel() throws Exception {

        ParcelLabelInput input = ParcelLabelInputTestData.domesticCOD();
        byte[] responseBody = mockMvc.perform(
                post("/labels/generate")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(input))
        ).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_PDF))
                .andReturn().getResponse().getContentAsByteArray();

        assertThat(isPDF(responseBody)).isTrue();
    }

    @Test
    void testInternationalLabel() throws Exception {

        ParcelLabelInput input = ParcelLabelInputTestData.international();
        byte[] responseBody = mockMvc.perform(
                post("/labels/generate")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(input))
        ).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_PDF))
                .andReturn().getResponse().getContentAsByteArray();

        assertThat(isPDF(responseBody)).isTrue();
    }

    @Test
    void testGenerateParcelWithCn23() throws Exception {

        ParcelWithCN23Input input = ParcelLabelInputTestData.parcelWithCN23Input();
        byte[] responseBody = mockMvc.perform(
                post("/labels-with-cn23/generate")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(input))
        ).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_PDF))
                .andReturn().getResponse().getContentAsByteArray();

        assertThat(isPDF(responseBody)).isTrue();
    }
}