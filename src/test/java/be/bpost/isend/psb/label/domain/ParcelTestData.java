package be.bpost.isend.psb.label.domain;

import java.math.BigDecimal;

public class ParcelTestData {
    public static Parcel singleCodParcel(){
        Parcel parcel = new Parcel();
        parcel.setBarcode("323205000014371031");
        parcel.setExtraWeight(false);
        parcel.setValidityDate("17-07-2017");
        parcel.setSender(AddressTestData.addressData());
        parcel.setReceiver(AddressTestData.addressData());
        parcel.setMaxWeight(1);
        parcel.setParcelType(ParcelType.DOMESTIC_PARCEL);
        parcel.setExtraWeight(true);
        parcel.setCaptureSignature(true);
        parcel.setCodAmount(new BigDecimal(123));
        parcel.setLabelLayout(ParcelLabelLayout.PREPAID_DOMESTIC);
        parcel.setCodAmount(new BigDecimal(100));
        parcel.setCodBarcode("C12345");
        parcel.setCodBankAccount("1122334455");

        return parcel;
    }

    public static Parcel singleCodParcel2(){
        Parcel parcel = new Parcel();
        parcel.setBarcode("323205000014371031");
        parcel.setExtraWeight(true);
        parcel.setValidityDate("17-07-2017");
        parcel.setSender(AddressTestData.addressData());
        parcel.setReceiver(AddressTestData.addressData());
        parcel.setMaxWeight(1);
        parcel.setParcelType(ParcelType.DOMESTIC_PARCEL);
        parcel.setExtraWeight(false);
        parcel.setCaptureSignature(true);
        parcel.setCodAmount(new BigDecimal(123));
        parcel.setLabelLayout(ParcelLabelLayout.PREPAID_DOMESTIC);
        parcel.setCodAmount(new BigDecimal(100));
        parcel.setCodBarcode("C12345");
        parcel.setCodBankAccount("1122334455");

        return parcel;
    }

    public static Parcel contractualInternational(){
        Parcel parcel = new Parcel();
        parcel.setBarcode("323205000014371031");
        parcel.setExtraWeight(true);
        parcel.setValidityDate("17-07-2017");
        parcel.setSender(AddressTestData.addressData());
        parcel.setReceiver(AddressTestData.addressData());
        parcel.setMaxWeight(1);
        parcel.setParcelType(ParcelType.DOMESTIC_PARCEL);
        parcel.setExtraWeight(false);
        parcel.setCaptureSignature(true);
        parcel.setLabelLayout(ParcelLabelLayout.CONTRACTUAL_INTL);
        parcel.setInternationalAddress(InternationalAddressTestData.internationalAddress());

        return parcel;
    }

}

