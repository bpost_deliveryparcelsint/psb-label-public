package be.bpost.isend.psb.label.domain;

import be.bpost.isend.psb.label.BeanPropertyTester;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

public class ProofOfPaymentInputTest {

    @Test
    public void testOrderNumber(){
        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        BeanPropertyTester.propertyTester(input, "orderNumber", String.class)
                .propertyIsMandatory()
                .withValidValues(input.getOrderNumber())
                .validate();
    }

   @Test
    public void testPaymentMethod(){
        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        BeanPropertyTester.propertyTester(input, "paymentMethod", String.class)
                .propertyIsMandatory()
                .withValidValues(input.getPaymentMethod())
                .validate();
    }

    @Test
    public void testDate(){
        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        BeanPropertyTester.propertyTester(input, "date", String.class)
                .propertyIsMandatory()
                .withValidValues(input.getDate())
                .validate();
    }

    @Test
    public void testSender(){
        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        BeanPropertyTester.propertyTester(input, "sender", Address.class)
                .propertyIsMandatory()
                .withValidValues(input.getSender())
                .validate();
    }

    @Test
    public void testProductDetails(){
        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        BeanPropertyTester.propertyTester(input, "productDetails", List.class)
                .propertyIsMandatory()
                .withValidValues(input.getProductDetails())
                .validate();
    }

    @Test
    public void testLanguage(){
        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        BeanPropertyTester.propertyTester(input, "language", Language.class)
                .propertyIsMandatory()
                .withValidValues(input.getLanguage())
                .validate();
    }

    @Test
    public void testVatNumber(){
        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        BeanPropertyTester.propertyTester(input, "vatNumber", String.class)
                .withValidValues(input.getVatNumber())
                .validate();
    }

    @Test
    public void testVatTotal(){
        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        BeanPropertyTester.propertyTester(input, "vatTotal", BigDecimal.class)
                .propertyIsMandatory()
                .withValidValues(input.getVatTotal())
                .validate();
    }
    @Test
    public void testVatInclusive(){
        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        BeanPropertyTester.propertyTester(input, "vatInclusive", BigDecimal.class)
                .propertyIsOptional()
                .withValidValues(input.getVatInclusive())
                .validate();
    }
    @Test
    public void testVatExclude(){
        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        BeanPropertyTester.propertyTester(input, "vatExclude", BigDecimal.class)
                .propertyIsMandatory()
                .withValidValues(input.getVatExclude())
                .validate();
    }
    @Test
    public void testTotalAmount(){
        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        BeanPropertyTester.propertyTester(input, "totalAmount", BigDecimal.class)
                .propertyIsOptional()
                .withValidValues(input.getTotalAmount())
                .validate();
    }
    @Test
    public void testDiscountAmount(){
        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        BeanPropertyTester.propertyTester(input, "discountAmount", BigDecimal.class)
                .propertyIsOptional()
                .withValidValues(input.getDiscountAmount())
                .validate();
    }
    @Test
    public void testCardNumber(){
        ProofOfPaymentInput input = ProofOfPaymentInputTestData.proofOfPaymentInputData();
        BeanPropertyTester.propertyTester(input, "cardNumber", String.class)
                .propertyIsOptional()
                .withValidValues(input.getCardNumber())
                .validate();
    }


}
