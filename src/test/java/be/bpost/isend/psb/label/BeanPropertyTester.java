package be.bpost.isend.psb.label;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.BeanWrapperImpl;

import javax.validation.*;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BeanPropertyTester<B, P> {

    private Validator validator;
    private B bean;
    private String propertyName;
    private List<P> validValues = Lists.newArrayList();
    private List<P> invalidValues = Lists.newArrayList();

    public BeanPropertyTester(Validator validator, B bean, String propertyName) {
        this.validator = validator;
        this.bean = bean;
        this.propertyName = propertyName;
    }

    public BeanPropertyTester(B bean, String propertyName) {
        this.bean = bean;
        this.propertyName = propertyName;
    }

    public static <B, P> BeanPropertyTester<B, P> propertyTester(Validator validator, B bean, String propertyName, Class<P> valueClass) {
        return new BeanPropertyTester<>(validator, bean, propertyName);
    }

    public static <B, P> BeanPropertyTester<B, P> propertyTester(B bean, String propertyName, Class<P> valueClass) {
        return new BeanPropertyTester<>(bean, propertyName);
    }

    public final BeanPropertyTester<B, P> withValidValues(P... values) {
        validValues.addAll(Arrays.asList(values));
        return this;
    }

    public BeanPropertyTester<B, P> withInvalidValues(P... values) {
        invalidValues.addAll(Arrays.asList(values));
        return this;
    }

    public BeanPropertyTester<B, P> propertyIsOptional() {
        validValues.add(null);
        return this;
    }

    public BeanPropertyTester<B, P> propertyIsMandatory() {
        invalidValues.add(null);
        return this;
    }

    public void validate() {
        // Test valid values
        validValues.forEach(this::assertValueIsValid);

        // Test invalid value
        invalidValues.forEach(this::assertValueIsInValid);
    }

    private Validator getValidator() {
        if (this.validator != null) {
            return this.validator;
        }
        return Validation.buildDefaultValidatorFactory().getValidator();
    }

    private <T> void validateBean(T bean) {
        Set<ConstraintViolation<T>> violations = getValidator().validate(bean);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }

    private void assertValueIsValid(P value) {
        setPropertyValue(value);
        try {
            validateBean(bean);
        } catch (ConstraintViolationException e) {
            Assertions.fail("Valid value [" + value + "] was rejected", e);
        }
    }

    private void assertValueIsInValid(P value) {
        setPropertyValue(value);
        ConstraintViolationException thrown = assertThrows(
                ConstraintViolationException.class, () -> validateBean(bean), "Expected value [" + value + "] to be rejected");
        assertThat(thrown.getMessage()).contains(propertyName);
    }

    private void setPropertyValue(P value) {
        BeanWrapperImpl wrappedBean = new BeanWrapperImpl(bean);
        wrappedBean.setPropertyValue(propertyName, value);
    }
}
