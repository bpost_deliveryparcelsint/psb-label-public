package be.bpost.isend.psb.label.service.catalog;

import be.bpost.isend.psb.label.domain.Language;
import be.bpost.isend.psb.order.openapi.psb.catalog.PSBProductCatalogItems;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
class CatalogDataCacheTest{

    @InjectMocks
    private CatalogDataCache catalogDataCache;

    @Mock
    private PsbCatalogService catalogService;

    @Test
    public void testCatalogDataCache() {

        PSBProductCatalogItems psbProductCatalogItems = configureSuccessfulCatalogCacheData();
        when(catalogService.getItems(Mockito.any(Language.class))).thenReturn(psbProductCatalogItems);
        assertNotNull(catalogDataCache.getCountryName("BE",Language.EN));
        assertNotNull(catalogDataCache.getShipmentTypes("OTHER",Language.EN));
        assertNotNull(catalogDataCache.getNonDeliveryInstructions("TREAT_AS_ABANDONED",Language.EN));
        assertTrue(catalogDataCache.isDestinationInEuropeanUnion("BE"));
    }

    private PSBProductCatalogItems configureSuccessfulCatalogCacheData() {
        return getCatalogCacheFromStub("CatalogCacheData.json");
    }

    private PSBProductCatalogItems getCatalogCacheFromStub(String s) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            URL url = this.getClass().getResource("/stubs/" + s);
            File file = new File(url.getFile());
            return objectMapper.readValue(file, PSBProductCatalogItems.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}