package be.bpost.isend.psb.label.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CN23DocumentInputTestData {

    public static CN23DocumentInput cn23DocumentInputData(){
        CN23DocumentInput cn23DocumentInput = new CN23DocumentInput();
        cn23DocumentInput.setPageFormat(PageFormat.A4);
        cn23DocumentInput.setNumOfCopies(4);
        List<CN23> cn23List = new ArrayList<>();
        cn23List.add(cn23Data());
        cn23DocumentInput.setCn23(cn23List);
        return cn23DocumentInput;
    }

    public static CN23 cn23Data(){
        CN23 cn23 = new CN23();
        cn23.setBarcode("3232000431");
        cn23.setTotalValue(new BigDecimal(100));
        cn23.setTotalGrossWeight(new BigDecimal(111));
        cn23.setPostalCharges(new BigDecimal(222));
        cn23.setShipmentType("OTHER");
        cn23.setOtherShipmentType("Fruits");
        cn23.setContentDescription("Description sdfafas asddfad ");
        cn23.setNonDeliveryInstruction("TREAT_AS_ABANDONED");
        cn23.setDate("11/22/3333");

        List<Item> itemList = new ArrayList<>();
        Item item = new Item();
        item.setItemDescription("Item Desc");
        item.setCountry("BE");
        item.setHsTariff("1");
        item.setQuantity(2);
        item.setWeight(new BigDecimal(1));
        item.setValue(new BigDecimal(1));
        itemList.add(item);

        cn23.setItems(itemList);

        Address sender = new Address();
        sender.setFirstName("SenderFirst");
        sender.setLastName("SenderLast");
        sender.setStreet("Sender Street");
        sender.setStreetNumber("1");
        sender.setCity("SenderCity");
        sender.setBox("A");
        sender.setPostalCode("100");
        sender.setCompany("Company");
        sender.setPhoneNum("1234567");
        sender.setCountry("BE");
        cn23.setSender(sender);

        Address receiver = new Address();
        receiver.setFirstName("ReceiverFirst");
        receiver.setLastName("ReceiverLast");
        receiver.setStreet("Receiver Street");
        receiver.setStreetNumber("2");
        receiver.setCity("ReceiverCity");
        receiver.setBox("B");
        receiver.setPostalCode("200");
        receiver.setCompany("Company");
        receiver.setPhoneNum("+32987654321");
        receiver.setEmail("receiver@test.com");
        receiver.setCountry("FR");
        receiver.setAddressOne("address one");
        receiver.setAddressTwo("address two");
        cn23.setReceiver(receiver);

        return cn23;
    }
}
