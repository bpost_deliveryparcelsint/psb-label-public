package be.bpost.isend.psb.label.domain;

import be.bpost.isend.psb.label.BeanPropertyTester;
import org.junit.jupiter.api.Test;

public class AddressTest {


    @Test
    public void testFirstName(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "firstName", String.class)
                .propertyIsOptional()
                .withValidValues(input.getFirstName())
                .validate();
    }

    @Test
    public void testLastName(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "lastName", String.class)
                .propertyIsOptional()
                .withValidValues(input.getLastName())
                .validate();
    }

    @Test
    public void testStreet(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "street", String.class)
                .propertyIsMandatory()
                .withValidValues(input.getStreet())
                .validate();
    }

    @Test
    public void testStreetNumber(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "streetNumber", String.class)
                .propertyIsOptional()
                .withValidValues(input.getStreetNumber())
                .validate();
    }

    @Test
    public void testBox(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "box", String.class)
                .propertyIsOptional()
                .withValidValues(input.getBox())
                .validate();
    }

    @Test
    public void testPostalCode(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "postalCode", String.class)
                .propertyIsMandatory()
                .withValidValues(input.getPostalCode())
                .validate();
    }

    @Test
    public void testCity(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "city", String.class)
                .propertyIsMandatory()
                .withValidValues("Brussels")
                .validate();
    }
///
    @Test
    public void testCountry(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "country", String.class)
            .propertyIsOptional()
            .withValidValues(input.getCountry())
            .validate();
    }

    @Test
    public void testPickupPoint(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "pickupPoint", String.class)
                .propertyIsOptional()
                .withValidValues(input.getPickupPoint())
                .validate();
    }

    @Test
    public void testParcelLocker(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "parcelLocker", String.class)
                .propertyIsOptional()
                .withValidValues(input.getParcelLocker())
                .validate();
    }

    @Test
    public void testCompany(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "company", String.class)
                .propertyIsOptional()
                .withValidValues(input.getCompany())
                .validate();
    }

    @Test
    public void testAddressOne(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "addressOne", String.class)
                .propertyIsOptional()
                .withValidValues(input.getAddressOne())
                .validate();
    }

    @Test
    public void testAddressTwo(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "addressTwo", String.class)
                .propertyIsOptional()
                .withValidValues(input.getAddressTwo())
                .validate();
    }

    @Test
    public void testPhoneNum(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "phoneNum", String.class)
                .propertyIsOptional()
                .withValidValues(input.getPhoneNum())
                .validate();
    }

    @Test
    public void testEmail(){
        Address input = AddressTestData.addressData();
        BeanPropertyTester.propertyTester(input, "email", String.class)
                .propertyIsOptional()
                .withValidValues(input.getEmail())
                .validate();
    }

}
