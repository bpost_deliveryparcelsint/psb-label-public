package be.bpost.isend.psb.label.domain;

import be.bpost.isend.psb.label.BeanPropertyTester;
import be.bpost.isend.psb.label.service.catalog.CatalogDataCache;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import javax.validation.Validator;
import java.util.List;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
class ParcelWithCN23InputTest {

    @Autowired
    private Validator validator;

    @MockBean
    private CatalogDataCache catalogDataCache;

    @BeforeEach
    public void configureCatalogData() {

        when(catalogDataCache.getShipmentTypes("OTHER",Language.EN)).thenReturn("Other");
        when(catalogDataCache.getShipmentTypes("COMMERCIAL_SAMPLE",Language.EN)).thenReturn("Commercial sample");
        when(catalogDataCache.getShipmentTypes("SALE_OF_GOODS",Language.EN)).thenReturn("Sale of goods");
        when(catalogDataCache.getShipmentTypes("GIFT",Language.EN)).thenReturn("Gift");
        when(catalogDataCache.getShipmentTypes("DOCUMENTS",Language.EN)).thenReturn("Documents");

        when(catalogDataCache.getNonDeliveryInstructions("TREAT_AS_ABANDONED",Language.EN)).thenReturn("Treat as abandoned");
        when(catalogDataCache.getNonDeliveryInstructions("RETURN_TO_SENDER_PRIORITY",Language.EN)).thenReturn("Return to sender (priority)");
        when(catalogDataCache.getNonDeliveryInstructions("RETURN_TO_SENDER_NON_PRIORITY",Language.EN)).thenReturn("Return to sender (non priority)");

        when(catalogDataCache.getCountryName("BE", Language.EN)).thenReturn("Belgium");
        when(catalogDataCache.getCountryName("FR", Language.EN)).thenReturn("France");
        when(catalogDataCache.getCountryName("CN", Language.EN)).thenReturn("China");
        when(catalogDataCache.getCountryName(
                argThat(countryCode -> !List.of("BE", "FR", "CN").contains(countryCode)),
                eq(Language.EN))
        ).thenThrow(new IllegalArgumentException("Invalid country code!"));
    }

    @Test
    void parcelWithCN23() {

        ParcelWithCN23Input input = ParcelWithCN23InputTestData.parcelWithCN23DataList();
        propertyTester(input, "parcelWithCN23", List.class)
                .propertyIsMandatory()
                .validate();
    }

    @Test
    public void testPageFormat() {
        ParcelWithCN23Input input = ParcelWithCN23InputTestData.parcelWithCN23DataList();
        propertyTester(input, "pageFormat", PageFormat.class)
                .propertyIsMandatory()
                .validate();
    }

    private <B, P> BeanPropertyTester<B, P> propertyTester(B bean, String propertyName, Class<P> valueClass) {
        return BeanPropertyTester.propertyTester(validator, bean, propertyName, valueClass);
    }
}