package be.bpost.isend.psb.label.report;

import be.bpost.isend.psb.label.domain.*;
import be.bpost.isend.psb.label.service.catalog.CatalogDataCache;
import be.bpost.isend.psb.label.util.AbstractMockitoTestCase;
import com.google.common.collect.Lists;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.output.OutputException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlunit.assertj3.XmlAssert;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static be.bpost.isend.psb.label.domain.ParcelLabelLayout.CONTRACTUAL_INTL;
import static be.bpost.isend.psb.label.report.LabelConstants.*;
import static org.mockito.Mockito.when;

class LabelUtilityTest extends AbstractMockitoTestCase {

    private static final Logger LOG = LoggerFactory.getLogger(LabelUtilityTest.class);

    @InjectMocks
    private LabelUtility service;

    @Mock
    private TemplateCache templateCache;

    @Mock
    private CatalogDataCache catalogDataCache;

    @BeforeEach
    void configureTemplates() {
        when(templateCache.getHtmlTemplate(HtmlTemplate.MAIN_TEMPLATE)).thenReturn(getMainTemplate());
        when(templateCache.getHtmlTemplate(HtmlTemplate.DOMESTIC_TEMPLATE)).thenReturn(getTemplate(HtmlTemplate.DOMESTIC_TEMPLATE));
        when(templateCache.getHtmlTemplate(HtmlTemplate.INTERNATIONAL_TEMPLATE)).thenReturn(getTemplate(HtmlTemplate.INTERNATIONAL_TEMPLATE));
        when(templateCache.getHtmlTemplate(HtmlTemplate.CN23_TEMPLATE)).thenReturn(getTemplate(HtmlTemplate.CN23_TEMPLATE));

        when(catalogDataCache.getNonDeliveryInstructions("TREAT_AS_ABANDONED", Language.EN)).thenReturn("Treat as abandoned");
        when(catalogDataCache.getShipmentTypes("OTHER", Language.EN)).thenReturn("Other");

        when(catalogDataCache.getCountryName("BE", Language.EN)).thenReturn("Belgium");
        when(catalogDataCache.getCountryName("CN", Language.EN)).thenReturn("China");
        when(catalogDataCache.getCountryName("FR", Language.EN)).thenReturn("France");
    }

    private String getMainTemplate() {
        StringBuffer sb = new StringBuffer("");
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        sb.append("<main_template>");
        sb.append(getVariable(TemplateVariable.CS_FIL));
        sb.append("<data>$labelData</data>");
        sb.append("</main_template>");
        return sb.toString();
    }

    private String getTemplate(HtmlTemplate template) {
        String tagName = template.name().toLowerCase();
        StringBuffer sb = new StringBuffer("");
        sb.append("<" + tagName + ">");
        TemplateVariable.getForTemplate(template).stream().forEach(v -> sb.append(getVariable(v)));
        sb.append("</" + tagName + ">");
        return sb.toString();
    }

    private String getVariable(TemplateVariable variable) {
        // Use a <![CDATA[ ]]> section so that HTML contents in variables does not interfere with XML parsing
        return "<var name=\"" + variable.getName() + "\"><![CDATA[$" + variable.getName() + "]]></var>";
    }

    @Test
    @SuppressWarnings("java:S5961") // Many assertions is acceptable here...
    void testDomesticAtHomeParcel() throws Exception {

        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneDomesticParcel();

        String xml = getFirstHtmlPage(parcelLabelInput);

        // Validate that correct CSS was selected for page size
        assertVariableValue(xml, TemplateVariable.CS_FIL, LabelConstants.PARCEL_A4_CSS);

        // Validate that the expected template was selected
        XmlAssert.assertThat(xml).hasXPath("/main_template/data/domestic_template");

        // Validate variables for domestic label
        assertVariableValue(xml, TemplateVariable.BAR_NUMBER, "323205000014371031");
        assertVariableValue(xml, TemplateVariable.VALIDITY_DATE, "17-07=2017");
        assertVariableValue(xml, TemplateVariable.MAX_WEIGHT, "1");
        assertVariableValue(xml, TemplateVariable.PARCEL_TYPE, ParcelType.DOMESTIC_PARCEL.displayName());
        assertVariableValue(xml, TemplateVariable.SENDER_HEADING, LabelTitle.SENDER.getDomestic());
        assertVariableValue(xml, TemplateVariable.RECEIVER_COUNTRY, LabelConstants.DOMESTIC_COUNTRY);
        assertVariableValue(xml, TemplateVariable.SENDER_COUN, LabelConstants.DOMESTIC_COUNTRY);
        assertVariableValue(xml, TemplateVariable.PHONE_NUM, "");
        assertVariableValue(xml, TemplateVariable.INTERNATIONAL_OUT, "");
        assertVariableValue(xml, TemplateVariable.COUN_CODE_STY, NONE);
        assertVariableValue(xml, TemplateVariable.SHOW_EXCLAMAITAION, LabelConstants.BLOCK);
        assertVariableValue(xml, TemplateVariable.SHOW_HIDE_EXTRA_WICHT, LabelConstants.BLOCK);

        // TODO Mock generation of barcode image so that it can be tested
        // assertVariableValue(xml, TemplateVariable.BAR_CODE_IMAGE, "ENCODED_BARCODE_IMAGE");

        // Sender data
        assertVariableValue(xml, TemplateVariable.SENDER_FIRST_NAME, "First Name");
        assertVariableValue(xml, TemplateVariable.SENDER_LAST_NAME, "Last Name");
        assertVariableValue(xml, TemplateVariable.SEN_COMPNAY, "");
        assertVariableValue(xml, TemplateVariable.SENDER_STREET, "Street");
        assertVariableValue(xml, TemplateVariable.SENDR_STRT_NUM, "23");
        assertVariableValue(xml, TemplateVariable.SENDER_BOX, " / 123<br>");
        assertVariableValue(xml, TemplateVariable.SENDER_POSTAL_CODE, "111222");
        assertVariableValue(xml, TemplateVariable.SENDER_CITY, "Brussels");

        // Receiver data
        assertVariableValue(xml, TemplateVariable.RECEIVER_FIRST_NAME, "First Name");
        assertVariableValue(xml, TemplateVariable.RECEIVER_LAST_NAME, "Last Name");
        assertVariableValue(xml, TemplateVariable.REC_STY, LabelConstants.REC_A4);
        assertVariableValue(xml, TemplateVariable.RECEIVER_STREET, "Street");
        assertVariableValue(xml, TemplateVariable.RECER_STRT_NUM, "23");
        assertVariableValue(xml, TemplateVariable.RECEIVER_BOX, " / 123<br>");
        assertVariableValue(xml, TemplateVariable.RECEIVER_POSTAL_CODE, "111222");
        assertVariableValue(xml, TemplateVariable.RECEIVER_CITY, "Brussels");

        // More product specific variables
        assertVariableValue(xml, TemplateVariable.KLANT, "");
        assertVariableValue(xml, TemplateVariable.VIA, BR);
        assertVariableValue(xml, TemplateVariable.LEFT_BLOCK, "");
        assertVariableValue(xml, TemplateVariable.HID_COD, NONE);
        assertVariableValue(xml, TemplateVariable.PCK_UP, "");
        assertVariableValue(xml, TemplateVariable.REC_COMPNAY, "");
    }

    @Test
    void testPageFormatA6() {

        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneDomesticParcel();
        parcelLabelInput.setPageFormat(PageFormat.A6);
        getFirstParcel(parcelLabelInput).getReceiver().setParcelLocker("Parcel Locker");

        String xml = getFirstHtmlPage(parcelLabelInput);

        // Validate that correct CSS was selected for page size
        assertVariableValue(xml, TemplateVariable.CS_FIL, LabelConstants.PARCEL_A6_CSS);
        assertVariableValue(xml, TemplateVariable.REC_STY, LabelConstants.REC_A6);
        assertVariableValue(xml, TemplateVariable.LEFT_BLOCK, LabelConstants.LOCKER_A6);
    }

    @Test
    void testCaptureSignature() {
        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneDomesticParcel();
        getFirstParcel(parcelLabelInput).setCaptureSignature(false);
        String xml = getFirstHtmlPage(parcelLabelInput);
        assertVariableValue(xml, TemplateVariable.SHOW_EXCLAMAITAION, NONE);
    }

    @Test
    void testExtraWeight() {
        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneDomesticParcel();
        getFirstParcel(parcelLabelInput).setExtraWeight(false);
        String xml = getFirstHtmlPage(parcelLabelInput);
        assertVariableValue(xml, TemplateVariable.SHOW_HIDE_EXTRA_WICHT, NONE);
    }

    @Test
    void testSenderCompany() {
        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneDomesticParcel();
        getFirstParcel(parcelLabelInput).getSender().setCompany("Sender Company");
        String xml = getFirstHtmlPage(parcelLabelInput);
        assertVariableValue(xml, TemplateVariable.SEN_COMPNAY, "Sender Company<br>");
    }

    @Test
    void testSenderCompanyIsTruncated() {
        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneDomesticParcel();
        getFirstParcel(parcelLabelInput).getSender().setCompany("Sender Company56789012345678901234567890");
        String xml = getFirstHtmlPage(parcelLabelInput);
        assertVariableValue(xml, TemplateVariable.SEN_COMPNAY, "Sender Company567890<br>");
    }

    @Test
    void testSenderBox() {
        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneDomesticParcel();
        getFirstParcel(parcelLabelInput).getSender().setBox(null);
        String xml = getFirstHtmlPage(parcelLabelInput);
        assertVariableValue(xml, TemplateVariable.SENDER_BOX, BR);
    }

    @Test
    void testReceiverCompany() {
        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneDomesticParcel();
        getFirstParcel(parcelLabelInput).getReceiver().setCompany("Receiver Company");
        String xml = getFirstHtmlPage(parcelLabelInput);
        assertVariableValue(xml, TemplateVariable.SEN_COMPNAY, "Receiver Company<br>");
    }

    @Test
    void testParcelLocker() {
        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneDomesticParcel();
        getFirstParcel(parcelLabelInput).getReceiver().setParcelLocker("Parcel Locker");
        getFirstParcel(parcelLabelInput).getReceiver().setCompany("Should not be shown");
        String xml = getFirstHtmlPage(parcelLabelInput);

        assertVariableValue(xml, TemplateVariable.LEFT_BLOCK, LabelConstants.LOCKER);
        assertVariableValue(xml, TemplateVariable.KLANT, "");
        assertVariableValue(xml, TemplateVariable.VIA, BR);
        assertVariableValue(xml, TemplateVariable.HID_COD, NONE);
        assertVariableValue(xml, TemplateVariable.PCK_UP, "Parcel Locker<br>");
        assertVariableValue(xml, TemplateVariable.REC_COMPNAY, "");
        assertVariableValue(xml, TemplateVariable.RECEIVER_STREET, "");
        assertVariableValue(xml, TemplateVariable.RECER_STRT_NUM, "");
        assertVariableValue(xml, TemplateVariable.RECEIVER_BOX, "");
    }

    @Test
    void testDomesticPickupPoint() {
        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneDomesticParcel();
        getFirstParcel(parcelLabelInput).getReceiver().setPickupPoint("Pickup Point");
        getFirstParcel(parcelLabelInput).getReceiver().setCompany("Should not be shown");
        String xml = getFirstHtmlPage(parcelLabelInput);

        assertVariableValue(xml, TemplateVariable.LEFT_BLOCK, LabelConstants.PUGO);
        assertVariableValue(xml, TemplateVariable.KLANT, LabelTitle.RECEIVER.getDomestic());
        assertVariableValue(xml, TemplateVariable.VIA, LabelTitle.VIA_PICK_UP_POINT.getDomestic());
        assertVariableValue(xml, TemplateVariable.HID_COD, NONE);
        assertVariableValue(xml, TemplateVariable.PCK_UP, "Pickup Point<br>");
        assertVariableValue(xml, TemplateVariable.REC_COMPNAY, "");
    }

    @Test
    void testDomesticPickupPointWithCOD() {
        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneDomesticParcel();
        getFirstParcel(parcelLabelInput).getReceiver().setPickupPoint("Pickup Point");
        getFirstParcel(parcelLabelInput).setCodBarcode("COD_BARCODE");
        getFirstParcel(parcelLabelInput).setCodAmount(new BigDecimal("29.99"));
        getFirstParcel(parcelLabelInput).setCodBankAccount("ABCD1234DEFG");
        String xml = getFirstHtmlPage(parcelLabelInput);

        assertVariableValue(xml, TemplateVariable.LEFT_BLOCK, LabelConstants.PUGO);
        assertVariableValue(xml, TemplateVariable.KLANT, LabelTitle.RECEIVER.getDomestic());
        assertVariableValue(xml, TemplateVariable.VIA, LabelTitle.VIA_PICK_UP_POINT.getDomestic());
        assertVariableValue(xml, TemplateVariable.HID_COD, BLOCK);
        assertVariableValue(xml, TemplateVariable.PCK_UP, "Pickup Point<br>");
        assertVariableValue(xml, TemplateVariable.REC_COMPNAY, "");

        assertVariableValue(xml, TemplateVariable.CDBAR, "C\tO\tD\t_\tB\tA\tR\tC\tO\tD\tE");
        // TODO Mock generation of barcode image
        // assertVariableValue(xml, TemplateVariable.COD_BAR_CODE, "ENCODED_COD_BARCODE");
        assertVariableValue(xml, TemplateVariable.AMOUNT, "29,99");
        assertVariableValue(xml, TemplateVariable.BANK_ACC, "ABCD 1234 DEFG");
        assertVariableValue(xml, TemplateVariable.CD_STY, COD_A4);

    }

    @Test
    void testInternationalPrepaidParcel() {
        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneInternational();

        String xml = getFirstHtmlPage(parcelLabelInput);

        assertVariableValue(xml, TemplateVariable.CATEGORY, "P");
        assertVariableValue(xml, TemplateVariable.EPG_LOGO, EPG_LOGO);
        assertVariableValue(xml, TemplateVariable.ADDITIONAL_TEXT, "");
        assertVariableValue(xml, TemplateVariable.LABEL_PRDUCT, "bpack Wordl business");
    }

    @Test
    void testInternationalPrepaidEconomy() {
        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneInternational();
        Parcel parcel = getFirstParcel(parcelLabelInput);
        parcel.setCategory("MAIL");
        parcel.getInternationalAddress().setProductName("International Economy");

        String xml = getFirstHtmlPage(parcelLabelInput);

        assertVariableValue(xml, TemplateVariable.CATEGORY, "U");
        assertVariableValue(xml, TemplateVariable.EPG_LOGO, "");
        assertVariableValue(xml, TemplateVariable.ADDITIONAL_TEXT, "");
        assertVariableValue(xml, TemplateVariable.LABEL_PRDUCT, "International Economy");
    }

    @Test
    void testInternationalRegisteredLetter() {
        ParcelLabelInput parcelLabelInput = ParcelLabelInputTestData.oneInternational();
        Parcel parcel = getFirstParcel(parcelLabelInput);
        parcel.setCategory("REGISTERED_LETTER");
        parcel.getInternationalAddress().setProductName("International Registered Letter");

        String xml = getFirstHtmlPage(parcelLabelInput);

        assertVariableValue(xml, TemplateVariable.CATEGORY, "R");
        assertVariableValue(xml, TemplateVariable.EPG_LOGO, "");
        assertVariableValue(xml, TemplateVariable.ADDITIONAL_TEXT, "");
        assertVariableValue(xml, TemplateVariable.LABEL_PRDUCT, "International Registered Letter");
    }

    @Test
    void testInternationalContractualParcel() {

        ParcelWithCN23Input input = ParcelLabelInputTestData.parcelWithCN23Input();

        String xml = getFirstHtmlPage(input);

        assertVariableValue(xml, TemplateVariable.CATEGORY, "P");
        assertVariableValue(xml, TemplateVariable.EPG_LOGO, EPG_LOGO);
        assertVariableValue(xml, TemplateVariable.ADDITIONAL_TEXT, "");
        assertVariableValue(xml, TemplateVariable.LABEL_PRDUCT, "bpack Wordl business");
    }

    @Test
    void testInternationalContractualExpressParcel() {

        ParcelWithCN23Input input = ParcelLabelInputTestData.parcelWithCN23Input();
        Parcel parcel = getFirstParcel(input);
        parcel.getInternationalAddress().seteParcelGroupLabel(false);
        parcel.getInternationalAddress().setProductName("Bpack World Express");

        String xml = getFirstHtmlPage(input);

        assertVariableValue(xml, TemplateVariable.CATEGORY, "P");
        assertVariableValue(xml, TemplateVariable.EPG_LOGO, "");
        assertVariableValue(xml, TemplateVariable.ADDITIONAL_TEXT, ADDITIONAL_TEXT_FOR_EXPRESS);
        assertVariableValue(xml, TemplateVariable.LABEL_PRDUCT, "Bpack World Express");
    }



/*

        label = label.replace("$codBarCode", getBarCodeImage(parcel.getCodBarcode()));
        label = label.replace("$amount", formatAmount(parcel.getCodAmount()));
        label = label.replace("$bankAcc", formatCODBankAccount(parcel.getCodBankAccount(),4));
        label = label.replace("$cdbar", formatCodBarCode(parcel.getCodBarcode()));
        label = label.replace("$cdSty", pageFormat.equals(PageFormat.A4) ? COD_A4: COD_A6);

        label = label.replace(LEFT_BLOCK, "");
        label = label.replace(KLANT, "");
        label = label.replace("$via", BR);
        label = label.replace(HIDE_COD, BLOCK);
        label = label.replace("$cdSty", pageFormat.equals(PageFormat.A4) ? COD_A4: COD_A6);
        label = label.replace(PICK_UP, "");
        label = label.replace(REC_COMPANY, Strings.isNullOrEmpty(receiver.getCompany()) ? "" : receiver.getCompany() + BR);


                } else if (!Strings.isNullOrEmpty(parcel.getCodBarcode())) { // Cash on Delivery : COD
                            label = LabelDataHelper.setCodDetails(pageFormat, parcel, label, receiver);

                    }

        // For International
        label = label.replace("$senderPhoneNumber", formatWithDelimiter(sender.getPhoneNum(),sender.getEmail()));
        label = label.replace("$senderEmail", Strings.isNullOrEmpty(sender.getEmail()) ? "" : sender.getEmail());

        if(parcel.getLabelLayout().equals(ParcelLabelLayout.PREPAID_INTL_PUGO)){
            label = label.replace("$parcelType", "");
            label = label.replace("$senderHeading", LabelTitle.SENDER.getInternational());
            label = label.replace("$counCodeSty", BLOCK);
        }
 */

    private Parcel getFirstParcel(ParcelLabelInput parcelLabelInput) {
        return parcelLabelInput.getParcels().get(0);
    }

    private Parcel getFirstParcel(ParcelWithCN23Input input) {
        return input.getParcelWithCN23().get(0).getParcel();
    }

    private String getFirstHtmlPage(ParcelLabelInput parcelLabelInput){
        List<String> htmlPages = generateHtmlPages(parcelLabelInput);
        return htmlPages.get(0);
    }

    private List<String> generateHtmlPages(ParcelLabelInput parcelLabelInput) {
        try {
            List<String> htmlPages = service.setParcelLabelValues(parcelLabelInput);
            htmlPages.forEach(htmlPage -> LOG.info("Page: {}", htmlPage));
            return htmlPages;
        } catch (BarcodeException|OutputException|IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String getFirstHtmlPage(ParcelWithCN23Input input){
        List<String> htmlPages = generateHtmlPages(input);
        return htmlPages.get(0);
    }

    private List<String> generateHtmlPages(ParcelWithCN23Input input) {
        try {
            List<String> htmlPages = service.setParcelWithCN23Values(input);
            htmlPages.forEach(htmlPage -> LOG.info("Page: {}", htmlPage));
            return htmlPages;
        } catch (BarcodeException|OutputException|IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void assertVariableValue(String xml, TemplateVariable variable, String expectedValue) {
        XmlAssert.assertThat(xml).valueByXPath("//var[@name=\"" + variable.getName() + "\"]/text()").isEqualTo(expectedValue);
    }

    @Test
    @Disabled("Not really a unit test, just a way to check which variables are present in a template")
    @SuppressWarnings("java:S2699")
    void extractVariables() throws Exception {


        String fileName = "/international_sub_label.html";
        String templateName = HtmlTemplate.INTERNATIONAL_TEMPLATE.name();

        String templatePath = "C:\\projects\\i_send\\psb\\psb-label-service\\src\\main\\resources\\" + fileName;
        String template = Files.readString(Path.of(templatePath), StandardCharsets.UTF_8);
        // LOG.info("Template:\n{}", template);

        Pattern pattern = Pattern.compile("(\\$\\w+)");
        Matcher matcher = pattern.matcher(template);
        List<String> variables = Lists.newArrayList();
        while (matcher.find()) {
            variables.add(matcher.group(1));
        }
        List<String> sortedVariables = variables.stream().distinct().sorted().collect(Collectors.toList());
        for (String variable : sortedVariables) {
            String variableName = variable.replace("$", "");
            String enumName = camelCaseToUnderScoreUpperCase(variableName);
            LOG.info("{}(HtmlTemplate." + templateName + ", \"{}\"),", enumName, variableName);
        }
    }

    public static String camelCaseToUnderScoreUpperCase(String camelCase) {
        String result = "";
        boolean prevUpperCase = false;
        for (int i = 0; i < camelCase.length(); i++) {
            char c = camelCase.charAt(i);
            if (!Character.isLetter(c))
                return camelCase;
            if (Character.isUpperCase(c)) {
                if (prevUpperCase)
                    return camelCase;
                result += "_" + c;
                prevUpperCase = true;
            } else {
                result += Character.toUpperCase(c);
                prevUpperCase = false;
            }
        }
        return result;
    }
}