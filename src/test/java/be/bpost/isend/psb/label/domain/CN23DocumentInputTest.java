package be.bpost.isend.psb.label.domain;

import be.bpost.isend.psb.label.BeanPropertyTester;
import be.bpost.isend.psb.label.service.catalog.CatalogDataCache;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import javax.validation.Validator;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
public class CN23DocumentInputTest {

    @Autowired
    private Validator validator;

    @MockBean
    private CatalogDataCache catalogDataCache;

    @BeforeEach
    public void configureCatalogData() {

        when(catalogDataCache.getShipmentTypes("OTHER",Language.EN)).thenReturn("Other");
        when(catalogDataCache.getShipmentTypes("COMMERCIAL_SAMPLE",Language.EN)).thenReturn("Commercial sample");
        when(catalogDataCache.getShipmentTypes("SALE_OF_GOODS",Language.EN)).thenReturn("Sale of goods");
        when(catalogDataCache.getShipmentTypes("GIFT",Language.EN)).thenReturn("Gift");
        when(catalogDataCache.getShipmentTypes("DOCUMENTS",Language.EN)).thenReturn("Documents");

        when(catalogDataCache.getNonDeliveryInstructions("TREAT_AS_ABANDONED",Language.EN)).thenReturn("Treat as abandoned");
        when(catalogDataCache.getNonDeliveryInstructions("RETURN_TO_SENDER_PRIORITY",Language.EN)).thenReturn("Return to sender (priority)");
        when(catalogDataCache.getNonDeliveryInstructions("RETURN_TO_SENDER_NON_PRIORITY",Language.EN)).thenReturn("Return to sender (non priority)");

        when(catalogDataCache.getCountryName("BE", Language.EN)).thenReturn("Belgium");
        when(catalogDataCache.getCountryName("FR", Language.EN)).thenReturn("France");
        when(catalogDataCache.getCountryName("CN", Language.EN)).thenReturn("China");
        when(catalogDataCache.getCountryName(
                argThat(countryCode -> !List.of("BE", "FR", "CN").contains(countryCode)),
                eq(Language.EN))
        ).thenThrow(new IllegalArgumentException("Invalid country code!"));
    }

    @Test
    public void testPageFormat() {
        CN23DocumentInput input = new CN23DocumentInput();
        input.setPageFormat(PageFormat.A4);
        BeanPropertyTester.propertyTester(input, "pageFormat", PageFormat.class)
                .propertyIsMandatory()
                .validate();
    }

    @Test
    public void testCN23() {

        CN23DocumentInput input = CN23DocumentInputTestData.cn23DocumentInputData();
        CN23 invalidCN23 = new CN23();
        propertyTester(input, "cn23", List.class)
                .propertyIsMandatory()
                .withInvalidValues(new ArrayList(), List.of(invalidCN23))
                .validate();
    }

    @Test
    public void testCN23Barcode() {
        CN23 cn23 = validCN23();
        propertyTester(cn23,"barcode",String.class)
                .propertyIsMandatory()
                .withInvalidValues("", " ")
                .withValidValues(cn23.getBarcode())
                .validate();
    }

    @Test
    public void testCN23Sender() {
        CN23 cn23 = validCN23();
        propertyTester(cn23, "sender", Address.class)
                .propertyIsMandatory()
                .withInvalidValues()
                .withValidValues(cn23.getSender())
                .validate();
    }

    @Test
    public void testCN23Receiver() {
        CN23 cn23 = validCN23();
        propertyTester(cn23, "receiver", Address.class)
                .propertyIsMandatory()
                .withInvalidValues()
                .withValidValues(cn23.getReceiver())
                .validate();
    }

    @Test
    public void testCN23Items(){
        CN23 cn23 = validCN23();
        propertyTester(cn23, "items", List.class)
                .propertyIsOptional()
                .withValidValues(cn23.getItems())
                .validate();
    }

    @Test
    public void testCN23totalValue(){
        CN23 cn23 = validCN23();
        propertyTester(cn23, "totalValue", BigDecimal.class)
                .propertyIsMandatory()
                .withValidValues(cn23.getTotalValue())
                .validate();
    }

    @Test
    public void testCN23totalGrossWeight(){
        CN23 cn23 = validCN23();
        propertyTester(cn23, "totalGrossWeight", BigDecimal.class)
                .propertyIsMandatory()
                .withValidValues(cn23.getTotalGrossWeight())
                .validate();
    }

    @Test
    public void testCN23postalCharges(){
        CN23 cn23 = validCN23();
        propertyTester(cn23, "postalCharges", BigDecimal.class)
                .propertyIsOptional()
                .withValidValues(cn23.getPostalCharges())
                .validate();
    }

    @Test
    public void testCN23shipmentType(){
        CN23 cn23 = validCN23();
        propertyTester(cn23, "shipmentType", String.class)
                .propertyIsMandatory()
                .withValidValues(cn23.getShipmentType())
                .validate();
    }

    @Test
    public void testCN23otherShipmentType(){
        CN23 cn23 = validCN23();
        propertyTester(cn23, "otherShipmentType", String.class)
                .propertyIsOptional()
                .withValidValues(cn23.getOtherShipmentType())
                .validate();
    }

    @Test
    public void testCN23contentDescription(){
        CN23 cn23 = validCN23();
        propertyTester(cn23, "contentDescription", String.class)
                .propertyIsMandatory()
                .withValidValues(cn23.getContentDescription())
                .validate();
    }

    @Test
    public void testCN23nonDeliveryInstruction(){
        CN23 cn23 = validCN23();
        propertyTester(cn23, "nonDeliveryInstruction", String.class)
                .propertyIsMandatory()
                .withValidValues(cn23.getNonDeliveryInstruction())
                .validate();
    }

    @Test
    public void testCN23date(){
        CN23 cn23 = validCN23();
        propertyTester(cn23, "date", String.class)
                .propertyIsMandatory()
                .withValidValues(cn23.getDate())
                .validate();
    }

   private CN23 validCN23(){
        return CN23DocumentInputTestData.cn23Data();
    }

    private <B, P> BeanPropertyTester<B, P> propertyTester(B bean, String propertyName, Class<P> valueClass) {
        return BeanPropertyTester.propertyTester(validator, bean, propertyName, valueClass);
    }
}
