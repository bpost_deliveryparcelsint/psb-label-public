package be.bpost.isend.psb.label.report;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static be.bpost.isend.psb.label.report.HtmlTemplate.DOMESTIC_TEMPLATE;
import static be.bpost.isend.psb.label.report.HtmlTemplate.MAIN_TEMPLATE;

public enum TemplateVariable {

    // Main template
    CS_FIL(MAIN_TEMPLATE, "csFil"),

    // Domestic template
    AMOUNT(DOMESTIC_TEMPLATE, "amount"),
    BANK_ACC(DOMESTIC_TEMPLATE, "bankAcc"),
    BAR_CODE_IMAGE(DOMESTIC_TEMPLATE, "barCodeImage"),
    BAR_NUMBER(DOMESTIC_TEMPLATE, "barNumber"),
    CD_STY(DOMESTIC_TEMPLATE, "cdSty"),
    CDBAR(DOMESTIC_TEMPLATE, "cdbar"),
    COD_BAR_CODE(DOMESTIC_TEMPLATE, "codBarCode"),
    COUN_CODE_STY(DOMESTIC_TEMPLATE, "counCodeSty"),
    DIV_STYLE(DOMESTIC_TEMPLATE, "divStyle"),
    HID_COD(DOMESTIC_TEMPLATE, "hidCod"),
    INT_COUNT_CODE(DOMESTIC_TEMPLATE, "intCountCode"),
    INTERNATIONAL_OUT(DOMESTIC_TEMPLATE, "internationalOut"),
    KLANT(DOMESTIC_TEMPLATE, "klant"),
    LEFT_BLOCK(DOMESTIC_TEMPLATE, "leftBlock"),
    MAX_WEIGHT(DOMESTIC_TEMPLATE, "maxWeight"),
    PARCEL_TYPE(DOMESTIC_TEMPLATE, "parcelType"),
    PCK_UP(DOMESTIC_TEMPLATE, "pckUp"),
    PHONE_NUM(DOMESTIC_TEMPLATE, "phoneNum"),
    REC_COMPNAY(DOMESTIC_TEMPLATE, "recCompnay"),
    REC_STY(DOMESTIC_TEMPLATE, "recSty"),
    RECEIVER_BOX(DOMESTIC_TEMPLATE, "receiverBox"),
    RECEIVER_CITY(DOMESTIC_TEMPLATE, "receiverCity"),
    RECEIVER_COUNTRY(DOMESTIC_TEMPLATE, "receiverCountry"),
    RECEIVER_FIRST_NAME(DOMESTIC_TEMPLATE, "receiverFirstName"),
    RECEIVER_LAST_NAME(DOMESTIC_TEMPLATE, "receiverLastName"),
    RECEIVER_POSTAL_CODE(DOMESTIC_TEMPLATE, "receiverPostalCode"),
    RECEIVER_STREET(DOMESTIC_TEMPLATE, "receiverStreet"),
    RECER_STRT_NUM(DOMESTIC_TEMPLATE, "recerStrtNum"),
    SEN_COMPNAY(DOMESTIC_TEMPLATE, "senCompnay"),
    SENDER_BOX(DOMESTIC_TEMPLATE, "senderBox"),
    SENDER_CITY(DOMESTIC_TEMPLATE, "senderCity"),
    SENDER_COUN(DOMESTIC_TEMPLATE, "senderCoun"),
    SENDER_FIRST_NAME(DOMESTIC_TEMPLATE, "senderFirstName"),
    SENDER_HEADING(DOMESTIC_TEMPLATE, "senderHeading"),
    SENDER_LAST_NAME(DOMESTIC_TEMPLATE, "senderLastName"),
    SENDER_POSTAL_CODE(DOMESTIC_TEMPLATE, "senderPostalCode"),
    SENDER_STREET(DOMESTIC_TEMPLATE, "senderStreet"),
    SENDR_STRT_NUM(DOMESTIC_TEMPLATE, "sendrStrtNum"),
    SHOW_EXCLAMAITAION(DOMESTIC_TEMPLATE, "showExclamaitaion"),
    SHOW_HIDE_EXTRA_WICHT(DOMESTIC_TEMPLATE, "showHideExtraWicht"),
    VALIDITY_DATE(DOMESTIC_TEMPLATE, "validityDate"),
    VIA(DOMESTIC_TEMPLATE, "via"),

    // International template (incomplete)
    CATEGORY(HtmlTemplate.INTERNATIONAL_TEMPLATE, "category"),
    ADDITIONAL_TEXT(HtmlTemplate.INTERNATIONAL_TEMPLATE, "additionalText"),
    EPG_LOGO(HtmlTemplate.INTERNATIONAL_TEMPLATE, "epgLogo"),
    LABEL_PRDUCT(HtmlTemplate.INTERNATIONAL_TEMPLATE, "labelPrduct"),

    ;



    private HtmlTemplate template;
    private String name;

    TemplateVariable(HtmlTemplate template, String name) {
        this.template = template;
        this.name = name;
    }

    public HtmlTemplate getTemplate() {
        return template;
    }

    public String getName() {
        return name;
    }

    public static List<TemplateVariable> getForTemplate(HtmlTemplate template) {
        return Arrays.stream(TemplateVariable.values())
                .filter(v -> v.getTemplate() == template)
                .collect(Collectors.toList());
    }
}
