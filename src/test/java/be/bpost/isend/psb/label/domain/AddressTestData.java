package be.bpost.isend.psb.label.domain;

public class AddressTestData {

    public static Address addressData(){

        Address address = new Address();
        address.setFirstName("First Name");
        address.setLastName("Last Name");
        address.setStreet("Street");
        address.setStreetNumber("23");
        address.setCity("Brussels");
        address.setBox("123");
        address.setPostalCode("111222");
        address.setCountry("BE");
        address.setPickupPoint("Pick Up Point");
        address.setParcelLocker("Parcel Locker");
        address.setCompany("Company Name");
        address.setAddressOne("Address One");
        address.setAddressTwo("Address Two");
        address.setPhoneNum("9876543210");
        address.setEmail("amolsanjay.bais.ext@bpost.be");

        return address;
    }


}
