package be.bpost.isend.psb.label.domain;

import be.bpost.isend.psb.label.BeanPropertyTester;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class ItemTest {

    @Test
    public void testItemDescription(){
        Item item = validItem();
        BeanPropertyTester.propertyTester(item, "itemDescription", String.class)
                .propertyIsMandatory()
                .withValidValues(item.getItemDescription())
                .validate();
    }

    @Test
    public void testItemQuantity(){
        Item item = validItem();
        BeanPropertyTester.propertyTester(item, "quantity", Integer.class)
                .propertyIsMandatory()
                .withValidValues(item.getQuantity())
                .validate();
    }

    @Test
    public void testItemWeight(){
        Item item = validItem();
        BeanPropertyTester.propertyTester(item, "weight", BigDecimal.class)
                .propertyIsMandatory()
                .withValidValues(item.getWeight())
                .validate();
    }

    @Test
    public void testItemValue(){
        Item item = validItem();
        BeanPropertyTester.propertyTester(item, "value", BigDecimal.class)
                .propertyIsMandatory()
                .withValidValues(item.getValue())
                .validate();
    }

    @Test
    public void testItemHsTariff(){
        Item item = validItem();
        BeanPropertyTester.propertyTester(item, "hsTariff", String.class)
                .propertyIsOptional()
                .withValidValues(item.getHsTariff())
                .validate();
    }

    @Test
    public void testItemCountry(){
        Item item = validItem();
        BeanPropertyTester.propertyTester(item, "country", String.class)
                .propertyIsMandatory()
                .withValidValues(item.getCountry())
                .validate();
    }

    private static Item validItem(){
        return ItemTestData.getItem();
    }
}
