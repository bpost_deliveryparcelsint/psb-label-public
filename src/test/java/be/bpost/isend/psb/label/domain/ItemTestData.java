package be.bpost.isend.psb.label.domain;

import java.math.BigDecimal;

public class ItemTestData {

    public static Item getItem(){
        Item item = new Item();
        item.setWeight(new BigDecimal(5));
        item.setValue(new BigDecimal(10));
        item.setQuantity(2);
        item.setHsTariff("20");
        item.setCountry("BE");
        item.setItemDescription("Content Description");
        return item;
    }
}
