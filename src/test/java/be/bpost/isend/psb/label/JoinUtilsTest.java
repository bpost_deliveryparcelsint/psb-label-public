package be.bpost.isend.psb.label;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class JoinUtilsTest {

    @Test
    void testFilterJoin() {
        assertThat(JoinUtils.filterJoin(" ")).isNull();
        assertThat(JoinUtils.filterJoin(" ", null, null)).isNull();
        assertThat(JoinUtils.filterJoin(" ", "String1", "String2")).isEqualTo("String1 String2");
        assertThat(JoinUtils.filterJoin(" ", " ", "String2")).isEqualTo("String2");
        assertThat(JoinUtils.filterJoin(" ", null, "String2")).isEqualTo("String2");
        assertThat(JoinUtils.filterJoin(" ", "String1", " ")).isEqualTo("String1");
        assertThat(JoinUtils.filterJoin(" ", "String1", null)).isEqualTo("String1");
        assertThat(JoinUtils.filterJoin("-", "String1", "String2", "String3")).isEqualTo("String1-String2-String3");
        assertThat(JoinUtils.filterJoin("-", "String1", null, "String3")).isEqualTo("String1-String3");
    }
}