package be.bpost.isend.psb.label.report;

import be.bpost.isend.psb.label.domain.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static be.bpost.isend.psb.label.report.LabelDataHelper.formatAmount;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class LabelDataHelperTest {

    @Test
    void testIsNumeric1(){
        assertFalse(LabelDataHelper.isNumeric(null));
    }

    @Test
    void testIsNumeric2(){
        assertFalse(LabelDataHelper.isNumeric(""));
    }

    @Test
    void testIsNumeric3(){
        assertFalse(LabelDataHelper.isNumeric("abc"));
    }

    @Test
    void testIsNumeric4(){
        assertTrue(LabelDataHelper.isNumeric("123"));
    }

    @Test
    void testGetBarCodeImage() throws Exception{
        String expected = "iVBORw0KGgoAAAANSUhEUgAAKOgAAAAiCAMAAABeWwsKAAADAFBMVEUAAAAAADMAAGYAAJkAAMwAAP8AMwAAMzMAM2YAM5kAM8wAM/8AZgAAZjMAZmYAZpkAZswAZv8AmQAAmTMAmWYAmZkAmcwAmf8AzAAAzDMAzGYAzJkAzMwAzP8A/wAA/zMA/2YA/5kA/8wA//8zAAAzADMzAGYzAJkzAMwzAP8zMwAzMzMzM2YzM5kzM8wzM/8zZgAzZjMzZmYzZpkzZswzZv8zmQAzmTMzmWYzmZkzmcwzmf8zzAAzzDMzzGYzzJkzzMwzzP8z/wAz/zMz/2Yz/5kz/8wz//9mAABmADNmAGZmAJlmAMxmAP9mMwBmMzNmM2ZmM5lmM8xmM/9mZgBmZjNmZmZmZplmZsxmZv9mmQBmmTNmmWZmmZlmmcxmmf9mzABmzDNmzGZmzJlmzMxmzP9m/wBm/zNm/2Zm/5lm/8xm//+ZAACZADOZAGaZAJmZAMyZAP+ZMwCZMzOZM2aZM5mZM8yZM/+ZZgCZZjOZZmaZZpmZZsyZZv+ZmQCZmTOZmWaZmZmZmcyZmf+ZzACZzDOZzGaZzJmZzMyZzP+Z/wCZ/zOZ/2aZ/5mZ/8yZ///MAADMADPMAGbMAJnMAMzMAP/MMwDMMzPMM2bMM5nMM8zMM//MZgDMZjPMZmbMZpnMZszMZv/MmQDMmTPMmWbMmZnMmczMmf/MzADMzDPMzGbMzJnMzMzMzP/M/wDM/zPM/2bM/5nM/8zM////AAD/ADP/AGb/AJn/AMz/AP//MwD/MzP/M2b/M5n/M8z/M///ZgD/ZjP/Zmb/Zpn/Zsz/Zv//mQD/mTP/mWb/mZn/mcz/mf//zAD/zDP/zGb/zJn/zMz/zP///wD//zP//2b//5n//8z///8SEhIYGBgeHh4kJCQqKiowMDA2NjY8PDxCQkJISEhOTk5UVFRaWlpgYGBmZmZsbGxycnJ4eHh+fn6EhISKioqQkJCWlpacnJyioqKoqKiurq60tLS6urrAwMDGxsbMzMzS0tLY2Nje3t7k5OTq6urw8PD29vb8/PwgKWLDAAACVklEQVR4Xu3asW0DQRAEQUnZKP9gmI7spz0CG4sqk9btPOj19+sL+n7ff/iQxf9lccviHQuLWyps+lTZY+HSpm7ZW9xScWlTtzwt3lFxaY/FLQv2eFrscemWBXs82eNpsUflloVLe1RuWbxjYXHLQmWPBZs+VfaoWHyXxaaVdywsblm4tEflloVLe1y6ZeHSHotbFi7t4Za9xS2XLL5LZdPFLRWVTRcW32WxR+UdC4tb4L/9vP8AAAAAAAAAAAAAUCF0BAAAAAAAAAAAALKEjgAAAAAAAAAAAECW0BEAAAAAAAAAAADIEjoCAAAAAAAAAAAAWUJHAAAAAAAAAAAAIEvoCAAAAAAAAAAAAGQJHQEAAAAAAAAAAIAsoSMAAAAAAAAAAACQJXQEAAAAAAAAAAAAsoSOAAAAAAAAAAAAQJbQEQAAAAAAAAAAAMgSOgIAAAAAAAAAAABZQkcAAAAAAAAAAAAgS+gIAAAAAAAAAAAAZAkdAQAAAAAAAAAAgCyhIwAAAAAAAAAAAJAldAQAAAAAAAAAAACyhI4AAAAAAAAAAABAltARAAAAAAAAAAAAyBI6AgAAAAAAAAAAAFlCRwAAAAAAAAAAACBL6AgAAAAAAAAAAABkCR0BAAAAAAAAAACALKEjAAAAAAAAAAAAkCV0BAAAAAAAAAAAALKEjgAAAAAAAAAAAECW0BEAAAAAAAAAAADIEjoCAAAAAAAAAAAAWUJHAAAAAAAAAAAAIEvoCAAAAAAAAAAAAGQJHQEAAAAAAAAAAIAsoSMAAAAAAAAAAACQ9QfQlSYbKQq9NgAAAABJRU5ErkJggg==";
        assertEquals(expected, LabelDataHelper.getBarCodeImage("323205001012781005"));
    }

    @Test
    void testFormatBox1(){
        assertEquals("",LabelDataHelper.formatBox(""));
    }

    @Test
    void testFormatBox2(){
        assertEquals(" / 1",LabelDataHelper.formatBox("1"));
    }

    @Test
    void testFormatCodBarCode(){
       assertEquals("1\t2\t3\t4\t5\t6",LabelDataHelper.formatCodBarCode("123456"));
    }

    @Test
    void testTruncateCompanyName(){
        assertEquals("ABC Company Pvt Ltd<br>",LabelDataHelper.truncateCompanyName("ABC Company Pvt Ltd"));
        assertEquals("",LabelDataHelper.truncateCompanyName(""));
    }

    @Test
    void testSetCodDetails() throws Exception{
        String expected = "iVBORw0KGgoAAAANSUhEUgAAICQAAAAiCAMAAACGGwRgAAADAFBMVEUAAAAAADMAAGYAAJkAAMwAAP8AMwAAMzMAM2YAM5kAM8wAM/8AZgAAZjMAZmYAZpkAZswAZv8AmQAAmTMAmWYAmZkAmcwAmf8AzAAAzDMAzGYAzJkAzMwAzP8A/wAA/zMA/2YA/5kA/8wA//8zAAAzADMzAGYzAJkzAMwzAP8zMwAzMzMzM2YzM5kzM8wzM/8zZgAzZjMzZmYzZpkzZswzZv8zmQAzmTMzmWYzmZkzmcwzmf8zzAAzzDMzzGYzzJkzzMwzzP8z/wAz/zMz/2Yz/5kz/8wz//9mAABmADNmAGZmAJlmAMxmAP9mMwBmMzNmM2ZmM5lmM8xmM/9mZgBmZjNmZmZmZplmZsxmZv9mmQBmmTNmmWZmmZlmmcxmmf9mzABmzDNmzGZmzJlmzMxmzP9m/wBm/zNm/2Zm/5lm/8xm//+ZAACZADOZAGaZAJmZAMyZAP+ZMwCZMzOZM2aZM5mZM8yZM/+ZZgCZZjOZZmaZZpmZZsyZZv+ZmQCZmTOZmWaZmZmZmcyZmf+ZzACZzDOZzGaZzJmZzMyZzP+Z/wCZ/zOZ/2aZ/5mZ/8yZ///MAADMADPMAGbMAJnMAMzMAP/MMwDMMzPMM2bMM5nMM8zMM//MZgDMZjPMZmbMZpnMZszMZv/MmQDMmTPMmWbMmZnMmczMmf/MzADMzDPMzGbMzJnMzMzMzP/M/wDM/zPM/2bM/5nM/8zM////AAD/ADP/AGb/AJn/AMz/AP//MwD/MzP/M2b/M5n/M8z/M///ZgD/ZjP/Zmb/Zpn/Zsz/Zv//mQD/mTP/mWb/mZn/mcz/mf//zAD/zDP/zGb/zJn/zMz/zP///wD//zP//2b//5n//8z///8SEhIYGBgeHh4kJCQqKiowMDA2NjY8PDxCQkJISEhOTk5UVFRaWlpgYGBmZmZsbGxycnJ4eHh+fn6EhISKioqQkJCWlpacnJyioqKoqKiurq60tLS6urrAwMDGxsbMzMzS0tLY2Nje3t7k5OTq6urw8PD29vb8/PwgKWLDAAAB80lEQVR4Xu3asRHCQBAEQSAb8g9G6WC/7KV0ddttyvpbCm/e1wvm+94/PCTxf0ncknhHwqZbprDpyR6nxB4JiU0Tt0x5xxT2yJuy6ZR3JCRuSZiyR8KmTafckjBlj8Q7EhK3TJHYNLFH4h2b2PS0aY9NtyQk9khIbLrplgR7nOxxSuyx6ZYppmyakPhdEntMeUdC4hb4t8/9AwAAAAAAAACwk0gAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACghEgAAAAAAAAAAEqIBAAAAAAAAACgxA/xLx0bpbZ8IwAAAABJRU5ErkJggg== 100,00 1122 3344 55 C\t1\t2\t3\t4\t5";
        String label = "$codBarCode $amount $bankAcc $cdbar";

        assertEquals(expected,LabelDataHelper.setCodDetails(PageFormat.A4, ParcelTestData.singleCodParcel(),label, AddressTestData.addressData()));
    }

    @Test
    void testGetDivStyle(){
        assertEquals(LabelConstants.DIV_1_STYLE,LabelDataHelper.getDivStyle(1));
        assertEquals(LabelConstants.DIV_2_STYLE,LabelDataHelper.getDivStyle(2));
        assertEquals(LabelConstants.DIV_3_STYLE,LabelDataHelper.getDivStyle(3));
        assertEquals(LabelConstants.DIV_4_STYLE,LabelDataHelper.getDivStyle(4));
        assertEquals(LabelConstants.DIV_A6_STYLE,LabelDataHelper.getDivStyle(5));
    }

    @Test
    void testFormatCODBankAccount(){
        assertEquals("BE 12 34 56 78",LabelDataHelper.formatCODBankAccount("BE12345678",2));
    }

    @Test
    void testFormatAmount() {
        assertThat(formatAmount(null)).isEqualTo("0,00");
        assertThat(formatAmount(new BigDecimal("123.99"))).isEqualTo("123,99");
        assertThat(formatAmount(new BigDecimal("123"))).isEqualTo("123,00");
        assertThat(formatAmount(new BigDecimal("123.1"))).isEqualTo("123,10");
        assertThat(formatAmount(new BigDecimal("-123.99"))).isEqualTo("-123,99");
        assertThat(formatAmount(new BigDecimal("123.99").negate())).isEqualTo("-123,99");
        assertThat(formatAmount(new BigDecimal("999999.99"))).isEqualTo("999999,99");
    }



    @Test
    void testFormatAmountInt(){
        assertEquals("0",LabelDataHelper.formatAmountInt(null));
    }

    @Test
    void testSetReceiverAddress(){
        String label = "$receiverFirstName $receiverLastName $recSty $receiverStreet $recerStrtNum $receiverBox $receiverPostalCode $receiverCity";
        String expectedLabel1 = "First Name Last Name float: left; margin-top: 3px; width: 100%;    111222 Brussels";
        String expectedLabel2 = "First Name Last Name float: left; margin-top: 0px; width: 100%; Street 23  / 123<br> 111222 Brussels";
        assertEquals(expectedLabel1,LabelDataHelper.setReceiverAddress(label,AddressTestData.addressData(),PageFormat.A4,ParcelLabelLayout.PREPAID_DOMESTIC));
        assertEquals(expectedLabel2,LabelDataHelper.setReceiverAddress(label,AddressTestData.addressData(),PageFormat.A6,ParcelLabelLayout.PREPAID_INTL));
    }

    @Test
    void testSetSenderAddress(){
        String label = "$senderFirstName $senderLastName $senCompnay $senderStreet $sendrStrtNum $senderBox $senderPostalCode $senderCity";
        String expected1 = "First Name Last Name Company Name<br> Street 23  / 123<br> 111222 Brussels";
        String expected2 = "First Name Last Name Company Name<br> Street 23  / 123<br> 111222 Brussels";

        assertEquals(expected1,LabelDataHelper.setSenderAddress(label,AddressTestData.addressData()));
    }

    @Test
    void testSetParcelInfo(){
        String label = "$barNumber $validityDate $maxWeight $parcelType $showExclamaitaion $showHideExtraWicht";
        String expected1 = "323205000014371031 17-07-2017 1 Nationaal Pakket | Paquet National | Nationales Paket block block";
        String expected2 = "323205000014371031 17-07-2017 1 Nationaal Pakket | Paquet National | Nationales Paket block none";

        assertEquals(expected1,LabelDataHelper.setParcelInfo(label,ParcelTestData.singleCodParcel()));
        assertEquals(expected2,LabelDataHelper.setParcelInfo(label,ParcelTestData.singleCodParcel2()));
    }

    @Test
    void testMethods(){
        String expectedFullName = "First Name Last Name";
        assertEquals(expectedFullName,LabelDataHelper.setFullName(AddressTestData.addressData(),30));
        assertEquals("",LabelDataHelper.setFullName(new Address(),30));

        String truncateData = "some data ";
        assertEquals(truncateData,LabelDataHelper.truncateData("some data for testing method",10));
        assertEquals("some ",LabelDataHelper.truncateData("some data for testing method",5));
    }

    @Test
    void formatAdditionalAddress(){
        assertEquals("addressOne - addressTwo",LabelDataHelper.formatWithDelimiter("addressOne","addressTwo"));
        assertEquals("addressOne",LabelDataHelper.formatWithDelimiter("addressOne",""));
        assertEquals("addressOne",LabelDataHelper.formatWithDelimiter("addressOne",null));
        assertEquals("addressTwo",LabelDataHelper.formatWithDelimiter("","addressTwo"));
        assertEquals("addressTwo",LabelDataHelper.formatWithDelimiter(null,"addressTwo"));
        assertEquals("",LabelDataHelper.formatWithDelimiter(null,null));
        assertEquals("",LabelDataHelper.formatWithDelimiter("",""));
    }

    @Test
    void formatContentDescription(){
        assertEquals("",LabelDataHelper.formatContentDescription(""));
        assertEquals("",LabelDataHelper.formatContentDescription(null));
        assertEquals("Content Description",LabelDataHelper.formatContentDescription("Content Description"));
        assertEquals("Some Content Description",LabelDataHelper.formatContentDescription("Some Content Description"));
        assertEquals("aaaaaaaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaaaaa",LabelDataHelper.formatContentDescription("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
        assertEquals("Content descriptionnnnnnn nnnnnnnnndescriptionnnnnn",LabelDataHelper.formatContentDescription("Content descriptionnnnnnnnnnnnnnnndescriptionnnnnn"));
    }

}