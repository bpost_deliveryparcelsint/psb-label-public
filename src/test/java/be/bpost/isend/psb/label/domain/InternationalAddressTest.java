package be.bpost.isend.psb.label.domain;

import be.bpost.isend.psb.label.BeanPropertyTester;
import org.junit.jupiter.api.Test;

class InternationalAddressTest {

    @Test
    void contentDescription(){
        InternationalAddress internationalAddress = InternationalAddressTestData.internationalAddress();
        BeanPropertyTester.propertyTester(internationalAddress, "contentDescription", String.class)
                .propertyIsOptional()
                .withValidValues(internationalAddress.getContentDescription())
                .validate();
    }

    @Test
    void accountId(){
        InternationalAddress internationalAddress = InternationalAddressTestData.internationalAddress();
        BeanPropertyTester.propertyTester(internationalAddress, "accountId", String.class)
                .propertyIsOptional()
                .withValidValues(internationalAddress.getAccountId())
                .validate();
    }

    @Test
    void shipmentType(){
        InternationalAddress internationalAddress = InternationalAddressTestData.internationalAddress();
        BeanPropertyTester.propertyTester(internationalAddress, "shipmentType", String.class)
                .propertyIsOptional()
                .withValidValues(internationalAddress.getShipmentType())
                .validate();
    }

    @Test
    void nonDeliveryInstruction(){
        InternationalAddress internationalAddress = InternationalAddressTestData.internationalAddress();
        BeanPropertyTester.propertyTester(internationalAddress, "nonDeliveryInstruction", String.class)
                .propertyIsOptional()
                .withValidValues(internationalAddress.getNonDeliveryInstruction())
                .validate();
    }

    @Test
    void productName(){
        InternationalAddress internationalAddress = InternationalAddressTestData.internationalAddress();
        BeanPropertyTester.propertyTester(internationalAddress, "productName", String.class)
                .propertyIsOptional()
                .withValidValues(internationalAddress.getProductName())
                .validate();
    }

}