package be.bpost.isend.psb.label.domain;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ProofOfPaymentInputTestData {

    public static ProofOfPaymentInput proofOfPaymentInputData(){

        Address address = new Address();
        address.setFirstName("First Name");
        address.setLastName("Last Name");
        address.setStreet("Street");
        address.setStreetNumber("23");
        address.setCity("Brussels");
        address.setBox("123");
        address.setPostalCode("111222");

        ProductDetails productDetails = new ProductDetails();
        List<ProductDetails> productDetailsList = new ArrayList<>();
        productDetails.setPricePerUnit(new BigDecimal(1));
        productDetails.setProductDescription("CUBEE");
        productDetails.setQuantity(1);
        productDetails.setTotalAmount(new BigDecimal(1));
        productDetailsList.add(productDetails);

        ProofOfPaymentInput paymentInputTestData = new ProofOfPaymentInput();
        paymentInputTestData.setOrderNumber("123456");
        paymentInputTestData.setPaymentMethod("KBC");
        paymentInputTestData.setDate("01-01-2020");
        paymentInputTestData.setSender(address);
        paymentInputTestData.setProductDetails(productDetailsList);
        paymentInputTestData.setLanguage(Language.EN);
        paymentInputTestData.setVatNumber("1");
        paymentInputTestData.setVatTotal(new BigDecimal(1));
        paymentInputTestData.setVatInclusive(new BigDecimal(1));
        paymentInputTestData.setTotalAmount(new BigDecimal(1));
        paymentInputTestData.setDiscountAmount(new BigDecimal(0.5));
        paymentInputTestData.setVatExclude(new BigDecimal(1));
        paymentInputTestData.setCardNumber("112233445566");

        return paymentInputTestData;
    }

}
