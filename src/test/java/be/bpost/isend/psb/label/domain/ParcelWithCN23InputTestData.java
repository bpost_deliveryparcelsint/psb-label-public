package be.bpost.isend.psb.label.domain;

import java.util.List;

public class ParcelWithCN23InputTestData {

    public static ParcelWithCN23 parcelWithCN23Data(){
        ParcelWithCN23 parcelWithCN23 = new ParcelWithCN23();
        parcelWithCN23.setParcel(ParcelTestData.contractualInternational());
        parcelWithCN23.setCn23(CN23DocumentInputTestData.cn23Data());
        return parcelWithCN23;
    }

    public static ParcelWithCN23Input parcelWithCN23DataList(){
        ParcelWithCN23Input parcelWithCN23Input = new ParcelWithCN23Input();
        parcelWithCN23Input.setPageFormat(PageFormat.A4);
        parcelWithCN23Input.setParcelWithCN23(List.of(parcelWithCN23Data()));

        return parcelWithCN23Input;
    }
}
