package be.bpost.isend.psb.label.domain;

import com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ParcelLabelInputTestData {

    public static ParcelLabelInput oneDomesticParcel() {
        ParcelLabelInput input = new ParcelLabelInput();
        List<Parcel> parcelList = new ArrayList<>();

        input.setPageFormat(PageFormat.A4);
        input.setParcels(parcelList);


        Address address = new Address();
        address.setFirstName("First Name");
        address.setLastName("Last Name");
        address.setStreet("Street");
        address.setStreetNumber("23");
        address.setCity("Brussels");
        address.setBox("123");
        address.setPostalCode("111222");

        Parcel parcel = new Parcel();
        parcel.setBarcode("323205000014371031");
        parcel.setExtraWeight(false);
        parcel.setValidityDate("17-07=2017");
        parcel.setSender(address);
        parcel.setReceiver(address);
        parcel.setMaxWeight(1);
        parcel.setParcelType(ParcelType.DOMESTIC_PARCEL);
        parcel.setExtraWeight(true);
        parcel.setCaptureSignature(true);
        parcel.setCodAmount(new BigDecimal(123));
        parcel.setInternationalAddress(InternationalAddressTestData.internationalAddress());
        parcel.setLabelLayout(ParcelLabelLayout.PREPAID_DOMESTIC);
        parcelList.add(parcel);


        return input;
    }

    public static ParcelLabelInput internationalPugo() {
        ParcelLabelInput input = new ParcelLabelInput();
        List<Parcel> parcelList = new ArrayList<>();

        input.setPageFormat(PageFormat.A4);
        input.setParcels(parcelList);


        Address address = new Address();
        address.setFirstName("First Name");
        address.setLastName("Last Name");
        address.setStreet("Street");
        address.setStreetNumber("23");
        address.setCity("FR");
        address.setBox("123");
        address.setPostalCode("111222");
        address.setCountry("CN");

        Parcel parcel = new Parcel();
        parcel.setBarcode("323205000014371031");
        parcel.setExtraWeight(false);
        parcel.setValidityDate("17-07-2020");
        parcel.setSender(address);
        parcel.setReceiver(address);
        parcel.setMaxWeight(1);
        parcel.setParcelType(ParcelType.DOMESTIC_PARCEL);
        parcel.setExtraWeight(false);
        parcel.setCaptureSignature(true);
        parcel.setLabelLayout(ParcelLabelLayout.PREPAID_INTL_PUGO);
        parcelList.add(parcel);


        return input;
    }

    public static ParcelLabelInput domesticCOD(){
        ParcelLabelInput input = new ParcelLabelInput();
        List<Parcel> parcelList = new ArrayList<>();

        input.setPageFormat(PageFormat.A4);
        input.setParcels(parcelList);


        Address address = new Address();
        address.setFirstName("First Name");
        address.setLastName("Last Name");
        address.setStreet("Street");
        address.setStreetNumber("23");
        address.setCity("FR");
        address.setBox("123");
        address.setPostalCode("111222");
        address.setCountry("CN");

        Parcel parcel = new Parcel();
        parcel.setBarcode("323205000014371031");
        parcel.setExtraWeight(false);
        parcel.setValidityDate("17-07-2020");
        parcel.setSender(address);
        parcel.setReceiver(address);
        parcel.setMaxWeight(1);
        parcel.setParcelType(ParcelType.CASH_ON_DELIVERY);
        parcel.setExtraWeight(false);
        parcel.setCaptureSignature(true);
        parcel.setLabelLayout(ParcelLabelLayout.PREPAID_DOMESTIC);

        // COD specific fields
        parcel.setCodAmount(new BigDecimal(100));
        parcel.setCodBankAccount("BE12345678");
        parcel.setCodBarcode("CE123456789");
        parcelList.add(parcel);

        return input;
    }

    public static ParcelLabelInput oneInternational() {

        ParcelLabelInput input = new ParcelLabelInput();
        input.setPageFormat(PageFormat.A4);
        List<Parcel> parcelList = new ArrayList<>();
        input.setParcels(parcelList);

        Address address = new Address();
        address.setFirstName("First Name");
        address.setLastName("Last Name");
        address.setStreet("Street");
        address.setStreetNumber("23");
        address.setCity("FR");
        address.setBox("123");
        address.setPostalCode("111222");
        address.setCountry("CN");
        address.setPhoneNum("987654321");
        address.setAddressOne("Address One");
        address.setAddressTwo("Address Two");

        Parcel parcel = new Parcel();
        parcel.setBarcode("323205000014371031");
        parcel.setExtraWeight(false);
        parcel.setValidityDate("17-07-2020");
        parcel.setSender(address);
        parcel.setReceiver(address);
        parcel.setMaxWeight(1);
        parcel.setParcelType(ParcelType.DOMESTIC_PARCEL);
        parcel.setExtraWeight(false);
        parcel.setCaptureSignature(true);
        parcel.setLabelLayout(ParcelLabelLayout.PREPAID_INTL);
        // international label
        parcel.setInternationalAddress(InternationalAddressTestData.internationalAddress());

        parcelList.add(parcel);

        return input;
    }

    public static ParcelLabelInput international() {
        ParcelLabelInput input = oneInternational();
        Parcel parcel = input.getParcels().get(0);
        input.getParcels().add(parcel);
        input.getParcels().add(parcel);
        input.getParcels().add(parcel);
        return input;
    }

    public static ParcelWithCN23Input parcelWithCN23Input(){
        ParcelWithCN23Input parcelWithCN23Input = new ParcelWithCN23Input();
        List<ParcelWithCN23> parcelWithCN23List = Lists.newArrayList();
        ParcelWithCN23 parcelWithCN23 = new ParcelWithCN23();

        Parcel parcel = ParcelTestData.contractualInternational();
        CN23 cn23 = CN23DocumentInputTestData.cn23Data();

        parcelWithCN23.setParcel(parcel);
        parcelWithCN23.setCn23(cn23);
        parcelWithCN23List.add(parcelWithCN23);
        parcelWithCN23Input.setParcelWithCN23(parcelWithCN23List);
        parcelWithCN23Input.setPageFormat(PageFormat.A4);

        return parcelWithCN23Input;
    }

}
