# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for the Label Generation service of the Parcel Sender Backbone.

### How do I get set up? ###

1. Build and run tests
2. Run service locally
3. Deployment

#### Build and run tests

    ./mvnw clean install

#### Run service locally

Run class `be.bpost.isend.psb.label.PsbLabelApplication` from your IDE.

[Spring-boot Dev Tools](https://docs.spring.io/spring-boot/docs/current/reference/html/using-spring-boot.html#using-boot-devtools)
are enabled so classes are reloaded automatically after they are built.